import React, { Component } from 'react';
import ChatBotLoader from '../ChatBotLoader/ChatBotLoader';
import './ChatBot.css';

/**
 * The chatbot component that contain the list of the stocked diagram models list and enable 
 * all it functionalities such as preview, edit, creat and delete.
 * @class ChatBot
 * @extends {Component}
 */
class ChatBot extends Component {
  render() {
    return (
      <div className="ChatBot">
        <ChatBotLoader />
      </div>
    );
  }
}

export default ChatBot;
