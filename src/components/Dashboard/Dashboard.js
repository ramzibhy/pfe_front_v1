import React, { Component } from 'react';
import axios from 'axios';
import Store from '../../store/store';
import AppNavBar from '../AppNavBar/AppNavBar';
import Home from '../Home/Home';
import ChatBot from '../ChatBot/ChatBot';
import ChatBotReviewer from '../ChatBotReviewer/ChatBotReviewer';
import Settings from '../Settings/Settings';
import DiagramDragDrop from '../DiagramDragDrop/DiagramDragDrop';
import { withStyles } from '@material-ui/core/styles';
import { Switch, Route } from 'react-router-dom';
import './Dashboard.css';

const styles = {
  '@global': {
    body: {
      backgroundColor: '#FFFFFF',
    },
  },
};

/**
 * Get all the diagram models stocked in a remote database and save it in 
 * a store app variable.
 */
const getDiagramModelList = () => {
  return axios.get(
    `${process.env.REACT_APP_BACKEND_WEBHOOK}/line/get_models`
  ).then((response) => {
     
    Store.dispatch({type: "setDiagramModelList", diagramModelList: response.data});
    /*
    console.log("requested")
    Object.keys(response.data).forEach((key) => {
      Store.dispatch({
        type: "addToDiagramModelList",
        diagramName: key,
        diagramModelJson: response.data[key]
      })
    })
    return "done";
    //  
    */
  });
}

/*
class Dashboard extends Component {
  render() {
    return (
      <div className="Dashboard">
        <AppNavBar />
      </div>
    );
  }
}

export default Dashboard;
*/

/**
 * The dashboard component.
 *
 * @returns { Component } 
 */
function Dashboard() {
  getDiagramModelList();
  return (
    <div>
      <AppNavBar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/chatbot" component={ChatBot} />
        <Route exact path="/chatbot_reviewer" component={ChatBotReviewer} />
        <Route exact path="/settings" component={Settings} />
        <Route exact path="/diagramdragdrop" component={DiagramDragDrop} />
      </Switch>
    </div>
  );
}

export default withStyles(styles)(Dashboard);