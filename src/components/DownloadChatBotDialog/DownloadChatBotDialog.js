import React, { Component } from 'react';
import Axios from 'axios';
import Store from '../../store/store';
import { download } from '../../scripts/downloadFile';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import './DownloadChatBotDialog.css';

const styles = {
  
}

/**
 * The DownloadChatBotDialog component.
 *
 * @class DownloadChatBotDialog
 * @extends {Component}
 */
class DownloadChatBotDialog extends Component {

  handleDownloadMavenProject = () => {
    let appStore = Store.getState();
    let downloadProjectJson = {
      ...this.props.getChatBotClassConfig(),
      diagramName: appStore.selectedDiagram
    }
    Axios({
      url: `${process.env.REACT_APP_BACKEND_WEBHOOK}/line/download_generated_project`,
      data: downloadProjectJson,
      method: 'post',
      config: {
        headers: { 'Content-Type': 'application/json' }
      },
    })
    .then((response) => {
      this.downloadFromUrl(response.data);
    })
  }

  downloadFromUrl = (uri) => {
    var link = document.createElement("a");
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  handleDownloadFile = () => {
    let classConfig = this.props.getChatBotClassConfig();
    download(classConfig.classContent, `${classConfig.className}.java`, "text/plain");
  }

  handleDialogClose = () => {
    this.props.changeDialogSwitchState(false);
  }

  render() {
    return (
      <div className="DownloadChatBotDialog">
        <Dialog
          open={this.props.dialogSwitchState}
          onClose={this.handleDialogClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth='xl'
        >
          <DialogContent>
            <div>
              <h3>Download Chatbot File</h3>
                <div>
                  This is the java class file that contain all the logic of the chatbot using the Line and DialogFlow Java dependency.<br></br>
                  In order to use it you have to:
                  <ol>
                    <li>Insert the file into your Spring project</li>
                    <li>Build your project</li>
                    <li>Insert your Chatbot controller webhook in DialogFlow</li>
                  </ol> 
                  To use it, just place it in your Spring boot project as a controller, and just use it webhook.
                </div>
              <br></br>
              <Button variant="outlined" color="primary" onClick={this.handleDownloadFile}>
                Download File
              </Button>
            </div>
            <hr></hr>
            <div>
              <h3>Download Chatbot Maven Project</h3>
              This is the whole Chatbot Maven Porject using Spring Boot.
              In order to use it you have to:
              <ol>
                <li>Extract the project zip file</li>
                <li>Build the maven project using the following command: "mvn package"</li>
                <li>Insert your Chatbot controller webhook in DialogFlow</li>
              </ol> 
              <Button variant="outlined" color="primary" onClick={this.handleDownloadMavenProject}>
                Download Maven Project
              </Button>
            </div>

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default DownloadChatBotDialog;
