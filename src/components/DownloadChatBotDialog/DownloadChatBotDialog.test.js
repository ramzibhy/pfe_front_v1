import React from 'react';
import ReactDOM from 'react-dom';
import DownloadChatBotDialog from './DownloadChatBotDialog';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DownloadChatBotDialog />, div);
  ReactDOM.unmountComponentAtNode(div);
});
