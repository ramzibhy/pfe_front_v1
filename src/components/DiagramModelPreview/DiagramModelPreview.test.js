import React from 'react';
import ReactDOM from 'react-dom';
import DiagramModelPreview from './DiagramModelPreview';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DiagramModelPreview />, div);
  ReactDOM.unmountComponentAtNode(div);
});
