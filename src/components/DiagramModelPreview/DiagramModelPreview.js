import React, { Component } from 'react';
import { connect } from 'react-redux';
import Store from '../../store/store';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import DiagramDragDrop from '../DiagramDragDrop/DiagramDragDrop';
import DiagramSample from '../DiagramSample/DiagramSample';

import './DiagramModelPreview.css';

/**
 * The Component that render the diagram model preview dialog (popup).
 *
 * @class DiagramModelPreview
 * @extends {Component}
 */
class DiagramModelPreview extends Component {

  constructor(props) {
    super(props);
  }

  handleClose = () => {
    Store.dispatch({
      type: 'setPreviewDiagramDialog',
      previewDiagramDialog: false,
    });
  }

  render() {
    return (
      <div>
        <Dialog
          open={this.props.previewDiagramDialog}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Diagram Preview"}</DialogTitle>
          <DialogContent>
            
            <DiagramSample />

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    previewDiagramDialog: state.previewDiagramDialog
  }
}

export default connect(mapStateToProps)(DiagramModelPreview);
