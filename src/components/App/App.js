import React, { Component } from 'react';
import { SnackbarProvider, withSnackbar } from 'notistack';
import Store from '../../store/store';
import './App.css';
import Dashboard from '../Dashboard/Dashboard';
import axios from 'axios';

/**
 * Load all the intents from the dialogflow with their intents and 
 * stock in a app store prop.
 * @param {String} dialogflowToken Dialogflow agent token
 */
const stockDialogFlowIntents = (dialogflowToken) => {
  let config = {
    headers: { 'Authorization': `Bearer ${dialogflowToken}` }
}
  axios.get(
    process.env.REACT_APP_DIALOGFLOW_INTENTS_WEBHOOK,
    config
  ).then((response) => {
    response.data.forEach((intent) => {
      let trainingPhrasesList = [];
      axios.get(
        `${process.env.REACT_APP_DIALOGFLOW_INTENTS_WEBHOOK}/${intent.id}`,
        config
      ).then((response) => {
        response.data.userSays.forEach((trainingPhrase) => {
          trainingPhrasesList.push(trainingPhrase.data[0].text);
        })
        Store.dispatch({
          type: "addToDialogFlowIntentList",
          intentID: response.data.id,
          intentName: response.data.name,
          trainingPhrasesList: trainingPhrasesList
        })
      })
    })
  })
}

/**
 *
 * This our front application main component, it will contain all the other components.
 * @class App
 * @extends {Component}
 */
class App extends Component {

  constructor(props) {
    super(props);
    // Load line chatbot config from the local storage
    let lineChatBotConfig = JSON.parse(localStorage.getItem('lineChatBotConfig'));
    // Load dialogflow config from the local storage
    let dialogFlowConfig = JSON.parse(localStorage.getItem('dialogFlowConfig'));
    Store.dispatch({
      type: "setAppComponent",
      appComponent: this,
    });
    // Update line chatbot config in not null
    if ( lineChatBotConfig !== null ) {
      Store.dispatch({
        type: "updateLineChatBotConfig",
        lineChatBotConfig
      })
    }
    // Update and load the dialogflow intent if the config in the storage is not null
    if ( dialogFlowConfig !== null ) {
      Store.dispatch({
        type: "updateDialogFlowConfig",
        dialogFlowConfig
      })
      stockDialogFlowIntents(dialogFlowConfig.dialflowAgentToken);
    }
  }

  render() {
    return (
      <div className="App">
        <Dashboard />
      </div>
    );
  }
}

const MyApp = withSnackbar(App);
/**
 *
 * Style the snackbars and implement it in the app compoent.
 * @returns { Component } The integration of the snackbar in the app component.
 */
function IntegrationNotistack() {
  const styles = {
    snackbar: {
      width: "400px",
    }
  };
  return <SnackbarProvider style = {styles.snackbar} maxSnack={3}>
    <MyApp />
  </SnackbarProvider>
}

export default IntegrationNotistack;
