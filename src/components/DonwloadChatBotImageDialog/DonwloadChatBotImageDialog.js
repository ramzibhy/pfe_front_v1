import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';

import ValidIcon from '@material-ui/icons/CheckCircleOutline';
import InvalidIcon from '@material-ui/icons/HighlightOff';
import UnknownIcon from '@material-ui/icons/HelpOutline';

import * as SockJS from "sockjs-client";
import { Stomp } from "@stomp/stompjs";

import './DonwloadChatBotImageDialog.css';

const styles = {
  loadingCircle: {
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "50px",
    display: "block"
  },
  generateProjectDiv: {
    marginRight: "auto",
    display: "inline"
  },
  deployProjectDiv: {
    display: "inline"
  },
  projectWebhookDiv: {
    marginLeft: "auto",
    display: "inline"
  },
  logBox: {
    marginTop: "20px",
    display: "block"
  },
  dialogBox: {
    width: "500px"
  }
}


/**
 * The DonwloadChatBotImageDialog component.
 *
 * @class DonwloadChatBotImageDialog
 * @extends {Component}
 */
class DonwloadChatBotImageDialog extends Component {

  state = {
    downloadImageLog = "",
  }

  connectWebSocket = () => {
    let socket = new SockJS(process.env.REACT_APP_BACKEND_URL_WEB_SOCKET);
    let stompClient = Stomp.over(socket);
    stompClient.connect({}, (frame) => {
       
      stompClient.subscribe('/user/topic/messages', (webSocketReply) => {
        this.handleWebSocketReply(JSON.parse(webSocketReply.body));
      });
      stompClient.send(
        process.env.REACT_APP_WEB_SOCKET_DOCKER_IMAGE_DOWNLOAD,
        {}, 
        JSON.stringify({
          "generatedProjectId": "whatever this is"
        })
      );

      /*
      this.setState({
        stompClient
      }, () => {
        stompClient.send(
          process.env.REACT_APP_WEB_SOCKET_PROJECT_DEPLOY,
          {}, 
          JSON.stringify(classConfig)
        );
      });
      */

    })
  }

  handleWebSocketReply = (data) => {
     
  }

  disconnectWebSocket = () => {
    if(this.state.stompClient != null) {
      this.state.stompClient.disconnect();
    }
     
  }

  handleDialogOpen = () => {
    this.connectWebSocket();
  }

  handleDialogClose = () => {
    this.disconnectWebSocket();
    this.props.changeDialogSwitchState(false);
  }

  render() {
    const { classes } = this.props;
    return (
      <div className="DonwloadChatBotImageDialog">
        <Dialog
          open={this.props.dialogSwitchState}
          onEntered={this.handleDialogOpen}
          onClose={this.handleDialogClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth='xl'
        >
          <DialogTitle id="alert-dialog-title">{"Deploying Project ..."}</DialogTitle>
          <DialogContent>

            <TextField
              id="outlined-multiline-static"
              className={classes.logBox}
              fullWidth={true}
              multiline
              rows="10"
              value={this.state.downloadImageLog}
              variant="outlined"
            />
          
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

DonwloadChatBotImageDialog.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DonwloadChatBotImageDialog);
