import React from 'react';
import ReactDOM from 'react-dom';
import GenerateChatBotClass from './GenerateChatBotClass';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GenerateChatBotClass />, div);
  ReactDOM.unmountComponentAtNode(div);
});
