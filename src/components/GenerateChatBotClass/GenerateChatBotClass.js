import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Store from '../../store/store';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import JsonDataModel from '../../scripts/jsonDataModel';
import { withStyles } from '@material-ui/core/styles';
import GeneratedChatBotClassesDialog from '../GeneratedChatBotClassesDialog/GeneratedChatBotClassesDialog';
import './GenerateChatBotClass.css';

const styles = {
  compressButton: {
    marginLeft: '10px',
    marginBottom: '10px',
    height: '49px',
  },
  toggleDialogButton: {
    marginLeft: '10px',
    marginBottom: '10px',
    height: '49px',
    color: 'white',
    background: '#2196F3'
  }
};

/**
 * The GenerateChatBotClass component.
 * It generate a new json from the gojs json and send it to the backend.
 * It will display the backend response in a dialog with the ability
 * to download the classes.
 * @class GenerateChatBotClass
 * @extends {Component}
 */
class GenerateChatBotClass extends Component {

  constructor(props) {
    super(props);
    this.state = {
      generatedClassesList: {},
      generatedClassesDialogSwitch: false,
      buttonDisplay: 'none',
    }
  }

  changeGeneratedClassesDialogSwitch = (switchStated) => {
    this.setState({
      generatedClassesDialogSwitch: switchStated,
    });
    if ( this.state.buttonDisplay !== 'inline' ) {
      this.setState({
        buttonDisplay: 'inline',
      })
    }
  }

  compressDiagramModel = () => {
    let appStore = Store.getState();
    try {
      if (appStore.imagesBeingUploaded !== 0) {
        throw "Wait until all images are done being uploaded";
      }
      let data = JsonDataModel(this.props.getDiagramModel());
      const configuredData = {
        'classes': data,
        'lineConfig': appStore.appSettings.lineChatBotConfig,
      }
       
      axios({
        url: `${process.env.REACT_APP_BACKEND_WEBHOOK}/line/compress_model`,
        data: configuredData,
        method: 'post',
        config: {
          headers: { 'Content-Type': 'application/json' }
        },
      })
      .then((response) => {
        //  
        this.setState({
          generatedClassesDialogSwitch: true,
          generatedClassesList: response.data,
        });
      })
    } catch(error) {
         
        appStore.appComponent.props.enqueueSnackbar(error, {variant: 'error'});
        throw error;
    }
    
    
  }

  render() {
    const { classes } = this.props;
    return (
      <div className="GenerateChatBotClass">
        <Button
          variant="outlined"
          className={classes.compressButton}
          onClick={this.compressDiagramModel}
        >
          Compress Diagram
        </Button>
        <Button
          className={classes.toggleDialogButton}
          style={
            {
              display: this.state.buttonDisplay
            }
          }
          onClick={() => this.changeGeneratedClassesDialogSwitch(true) }
        >
          Generated ChatBot Classes
        </Button>
        <GeneratedChatBotClassesDialog
          dialogSwitchState = {this.state.generatedClassesDialogSwitch}
          changeDialogSwitchState = {this.changeGeneratedClassesDialogSwitch}
          generatedClassesList = {this.state.generatedClassesList}
        />
      </div>
    );
  }
}

GenerateChatBotClass.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GenerateChatBotClass);
