import React from 'react';
import ReactDOM from 'react-dom';
import DiagramDragDrop from './DiagramDragDrop';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DiagramDragDrop />, div);
  ReactDOM.unmountComponentAtNode(div);
});
