import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as go from 'gojs';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import SaveDiagramModel from '../SaveDiagramModel/SaveDiagramModel';
import DialogFlowIntentFormComponent from '../DialogFlowIntentFormComponent/DialogFlowIntentFormComponent';
import GenerateChatBotClass from '../GenerateChatBotClass/GenerateChatBotClass';

import BotControllerClassNode from "../../nodes/botControllerClassNode";
import DialogflowintentNode from "../../nodes/dialogflowIntentNode";
import TemplateMessageNode from "../../nodes/templateMessageNode";
import CarouselTemplateNode from "../../nodes/carouselTemplateNode";
import CarouselColumnNode from "../../nodes/carouselColumnNode";
import ButtonTemplateNode from "../../nodes/buttonsTemplateNode";
import ConfirmTemplateNode from "../../nodes/confirmTemplateNode";
import TextMessageNode from "../../nodes/textMessageNode";
import MessageActionNode from "../../nodes/messageActionNode";
import UriActionNode from "../../nodes/uriActionNode";
import PostBackActionNode from "../../nodes/postBackActionNode";
import './DiagramDragDrop.css';

/**
 * The Component that render the diagram drag and drop system.
 *
 * @class DiagramDragDrop
 * @extends {Component}
 */
class DiagramDragDrop extends Component {

  constructor(props) {
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      myDiagram: null,
    }
  }

  getDiagramModel = () => {
    return this.state.myDiagram.model.toJson();
  }

  componentDidMount() {
    const { newDiagram } = this.props.location.state;
    const $ = go.GraphObject.make;
    const myDiagram = $(go.Diagram, "myDiagramDiv", 
      {
      "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
        initialContentAlignment: go.Spot.Center,
      }
    );
    this.setState({myDiagram});
    
    myDiagram.nodeTemplateMap.add("BotControllerClassNode", BotControllerClassNode())
    myDiagram.nodeTemplateMap.add("DialogflowintentNode",DialogflowintentNode());
    myDiagram.nodeTemplateMap.add("TemplateMessageNode", TemplateMessageNode());
    myDiagram.nodeTemplateMap.add("CarouselTemplateNode", CarouselTemplateNode());
    myDiagram.nodeTemplateMap.add("CarouselColumnNode", CarouselColumnNode(this));
    myDiagram.nodeTemplateMap.add("ButtonTemplateNode", ButtonTemplateNode(this));
    myDiagram.nodeTemplateMap.add("ConfirmTemplateNode", ConfirmTemplateNode());
    myDiagram.nodeTemplateMap.add("TextMessageNode", TextMessageNode());
    myDiagram.nodeTemplateMap.add("MessageActionNode", MessageActionNode());
    myDiagram.nodeTemplateMap.add("UriActionNode", UriActionNode());
    myDiagram.nodeTemplateMap.add("PostBackActionNode", PostBackActionNode());
    
    if(newDiagram || newDiagram == null) {
      myDiagram.model = new go.GraphLinksModel();
    } else {
      myDiagram.model = go.Model.fromJson(this.props.diagramModelList[this.props.selectedDiagram]);
    }

    let myPaletteNodesList = [
      { category: "BotControllerClassNode" },
      { category: "DialogflowintentNode", commands: [
          { text: "Config Intent", action: "config" },
          ]
      },
      { category: "TextMessageNode" },
      { category: "TemplateMessageNode" },
      { category: "MessageActionNode" },
      { category: "UriActionNode" },
      { category: "PostBackActionNode" },
      { category: "CarouselTemplateNode" },
      { category: "ButtonTemplateNode", commands: [
          { text: "Change Image", action: "change" },
          { text: "Reset Default Image", action: "reset" }]
      },
      { category: "ConfirmTemplateNode" },
      { category: "CarouselColumnNode", commands: [
        { text: "Change Image", action: "change" },
        { text: "Reset Default Image", action: "reset" }]
      },
    ];

    const myPalette = $(go.Palette, "myPaletteDiv",  // must name or refer to the DIV HTML element
      {
        nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
        model: new go.GraphLinksModel(myPaletteNodesList)
      }
    )
  }

  render() {
    const { newDiagram } = this.props.location.state;
     
    return (
      <div>
        <form>
          <input className="uploadImageField" type="file" id="fileUpload"/>
          <input className="uploadImageField" type="file" id="fileUpload2"/>
        </form>
        <SaveDiagramModel diagramName={this.props.selectedDiagram} newDiagram={newDiagram} getDiagramModel={this.getDiagramModel} />
        <GenerateChatBotClass getDiagramModel={this.getDiagramModel} />
        <DialogFlowIntentFormComponent />
        <div className="DiagramDragDrop">
          <div className='myGraph'>
            <div id="myPaletteDiv" className='myPalette'></div>
            <div id="myDiagramDiv" className='myDiagram'></div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    diagramModelList: state.diagramModelList,
    selectedDiagram: state.selectedDiagram,
  }
}

export default connect(mapStateToProps)(DiagramDragDrop);
