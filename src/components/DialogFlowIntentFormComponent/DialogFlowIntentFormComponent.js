import React, { Component } from 'react';
import { connect } from 'react-redux';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import store from "../../store/store";
import './DialogFlowIntentFormComponent.css';

/**
 * The DialogFlowIntentForm component.
 * Allow the user to add or edit training phrases for the selected dialog flow intent.
 * @class DialogFlowIntentFormComponent
 * @extends {Component}
 */
class DialogFlowIntentFormComponent extends Component {
  
  constructor (props) {
    super(props);
    this.state = {
      trainingPhrasesComponentList: [],
      trainingPhrasesTextList: [],
      listLength: 0
    }
  }

  handleTrainingPhraseTextChange = (event, index) => {
    let trainingPhrasesTextListUpdated = this.state.trainingPhrasesTextList;
    trainingPhrasesTextListUpdated[index] = event.target.value;
    this.setState({
        trainingPhrasesTextList: trainingPhrasesTextListUpdated
    });
  }

  trainingPhraseTemplate = (trainingPhraseIndex, trainingPhrase) => {
    return (
        <div className="flexDiv" key={`training_phrase_${trainingPhraseIndex}`}>
            <TextField
            // autoFocus
            key={`set_training_phrase_${trainingPhraseIndex}`} 
            label="Training Phrase"
            // value={this.state.trainingSpeechTextList[trainingSpeechIndex]}
            defaultValue={trainingPhrase}
            margin="none"
            variant="outlined"
            fullWidth
            onChange={(e) => {this.handleTrainingPhraseTextChange(e, trainingPhraseIndex)}}
            />
            <IconButton aria-label="Delete" onClick={() => {this.removeTrainingPhrase(trainingPhraseIndex)}}>
            <DeleteIcon fontSize="large" />
            </IconButton>
        </div>
    );
  }

  addNewTrainingPhrase = (trainingPhrase) => {
    let trainingPhraseIndex = this.state.listLength;
     
    this.setState({
        trainingPhrasesTextList: [...this.state.trainingPhrasesTextList, trainingPhrase]
    }, () => {
         
        this.setState({
            trainingPhrasesComponentList: [...this.state.trainingPhrasesComponentList,
                this.trainingPhraseTemplate(trainingPhraseIndex, trainingPhrase)
            ]
        });
        this.setState({
            listLength: this.state.listLength + 1
        })
    })
  }

  addDialogflowTrainingPhrases = (trainingPhrasesList) => {
    //  
    //  
    let trainingPhrasesListCopy = [...trainingPhrasesList];
    let dialogflowTrainingPhrasesComponentList = [];
    for (let index = 0; index < trainingPhrasesListCopy.length; index++) {
        dialogflowTrainingPhrasesComponentList.push(this.trainingPhraseTemplate(index, trainingPhrasesListCopy[index]));
    }
    this.setState({
        trainingPhrasesComponentList: dialogflowTrainingPhrasesComponentList,
        trainingPhrasesTextList: trainingPhrasesListCopy,
        listLength: trainingPhrasesListCopy.length
    })
  }

  handleClose = () => {
    store.dispatch({type: "setDialogFlowConfigDialog", dialogFlowConfigDialog: false});
  }

  handleOpen = (intentName) => {
    this.setState({
        listLength: 0,
        trainingPhrasesComponentList: [],
        trainingPhrasesTextList: []
    }, () => {
         
        let intentNameList = Object.keys(this.props.dialogFlowIntentList);
         
        for (let index = 0; index < intentNameList.length; index++) {
            if(intentNameList[index] === intentName) {
                 
                this.addDialogflowTrainingPhrases(
                    this.props.dialogFlowIntentList[intentNameList[index]]["trainingPhrases"]
                );
                break;
            }
            
        }
    })
  }

  removeTrainingPhrase = (index) => {
     
    let trainingPhrasesComponentListUpdated = this.state.trainingPhrasesComponentList;
    let trainingPhrasesTextListUpdated = this.state.trainingPhrasesTextList;

    trainingPhrasesComponentListUpdated[index] = null;
    trainingPhrasesTextListUpdated[index] = "";

    this.setState({
        trainingPhrasesComponentList: trainingPhrasesComponentListUpdated,
        trainingPhrasesTextList: trainingPhrasesTextListUpdated
    })
     
     
  }
  
  confirmIntentConfig = () => {
    store.dispatch({
        type: "addToUpdatedIntentList",
        intentName: this.props.selectedDialogFlowIntent,
        // remove the empty training phrases
        trainingPhrasesList: this.state.trainingPhrasesTextList.filter(e => e !== "")
    })
    this.handleClose();
     
  }
  
  render() {
    return (
      <div>
        <Dialog
            open={this.props.dialogFlowConfigDialog}
            onEntering={() => {this.handleOpen(this.props.selectedDialogFlowIntent)}}
            maxWidth="sm"
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle id="form-dialog-title">Intent Configurations:</DialogTitle>
            <DialogContent id="dialogContent">
                <DialogContentText>
                    Set the configuration of a new intent or update that of an old one.
                </DialogContentText>
                <Button variant="contained" size="small" color="primary" onClick={()=> {this.addNewTrainingPhrase("test")}}>
                    Add Training Speech
                </Button>
                <div><br /></div>
                {
                    this.state.trainingPhrasesComponentList
                }
            </DialogContent>
            <DialogActions>
                <Button color="secondary" variant="outlined" onClick={()=>{ this.handleClose() }}>
                Cancel
                </Button>
                <Button variant="outlined" onClick={()=>{ this.confirmIntentConfig() }}>
                Confirm
                </Button>
            </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dialogFlowIntentList: state.dialogFlowIntentList,
    dialogFlowConfigDialog: state.dialogFlowConfigDialog,
    selectedDialogFlowIntent: state.selectedDialogFlowIntent

  }
}

export default connect(mapStateToProps)(DialogFlowIntentFormComponent);
