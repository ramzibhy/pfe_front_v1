import React from 'react';
import ReactDOM from 'react-dom';
import DialogFlowIntentFormComponent from './DialogFlowIntentFormComponent';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DialogFlowIntentFormComponent />, div);
  ReactDOM.unmountComponentAtNode(div);
});
