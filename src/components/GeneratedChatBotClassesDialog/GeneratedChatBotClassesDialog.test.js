import React from 'react';
import ReactDOM from 'react-dom';
import GeneratedChatBotClassesDialog from './GeneratedChatBotClassesDialog';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GeneratedChatBotClassesDialog />, div);
  ReactDOM.unmountComponentAtNode(div);
});
