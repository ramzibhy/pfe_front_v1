import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import SwipeableViews from 'react-swipeable-views';
import TextField from '@material-ui/core/TextField';
import Store from '../../store/store';
import Axios from 'axios';

import { download } from '../../scripts/downloadFile';
import DeployChatBotDialog from '../DeployChatBotDialog/DeployChatBotDialog';
import DownloadChatBotDialog from '../DownloadChatBotDialog/DownloadChatBotDialog';
import * as SockJS from "sockjs-client";
import { Stomp } from "@stomp/stompjs";

import './GeneratedChatBotClassesDialog.css';


/*
const styles = {
  
}
*/

/**
 * The GeneratedChatBotClassesDialog component.
 * The dialog that present for the user the code the classes that have been generated.
 * @class GeneratedChatBotClassesDialog
 * @extends {Component}
 */
class GeneratedChatBotClassesDialog extends Component {

  state = {
    value: 0,
    deployDialogState: false,
    downloadDialogState: false,
    /*
    stompClient: null,
    // stages: generate -> deploy -> webhook
    deployProjectStages: {
      generateProjectIcon: "unknown",
      deployProjectIcon: "unknown",
      projectWebhookIcon: "unknown"
    },
    deployProjectProgressMessage: "Generating project",
    */
  };

  getChatBotClassConfig = () => {
    let className = Object.keys(this.props.generatedClassesList)[this.state.value];
    let classContent = this.props.generatedClassesList[className];
    let lineConfig = Store.getState().appSettings.lineChatBotConfig;
    return {
      className,
      classContent,
      lineConfig
    }
  }

  /*
  connectWebSocket = () => {
    let socket = new SockJS(process.env.REACT_APP_BACKEND_URL_WEB_SOCKET);
     
    let stompClient = Stomp.over(socket);
    stompClient.connect({}, (frame) => {
       
      stompClient.subscribe('/user/topic/messages', (webSocketReply) => {
         
        this.handleWebSocketReply(JSON.parse(webSocketReply.body));
      });
      this.setState({
        stompClient
      });
      setTimeout(()=> {
        let className = Object.keys(this.props.generatedClassesList)[this.state.value];
        let classContent = this.props.generatedClassesList[className];
        let lineConfig = Store.getState().appSettings.lineChatBotConfig;
        stompClient.send(process.env.REACT_APP_WEB_SOCKET_PROJECT_DEPLOY, {}, JSON.stringify(
          {
            className,
            classContent,
            lineConfig
          }
        ));
      }, 3000)
    })
  }

  disconnectWebSocket = () => {
    if(this.state.stompClient != null) {
      this.stompClient.disconnect();
    }
     
  }

  handleWebSocketReply = (webSocketReply) => {
     
    /*
    this.setState({
      deployProjectProgressMessage: webSocketReply.data
    })
    
  }
  */
  handleTabChange = (event, value) => {
    this.setState({
      value
    });
  }

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  handleDialogClose = () => {
    this.props.changeDialogSwitchState(false);
  }

  changeDeployDialogState = (state) => {
    this.setState({
      deployDialogState: state,
    })
  }

  changeDownloadDialogState = (state) => {
    this.setState({
      downloadDialogState: state,
    })
  }

  generateClassesTabs = () => {
    let tabs = [];
    let tabContainers = [];
    Object.keys(this.props.generatedClassesList).forEach((className) => {
      tabs.push(<Tab key={className} label={className} />);
      let classHtmlContent = this.props.generatedClassesList[className]
      tabContainers.push(
        <TextField
          key={className}
          id="outlined-multiline-flexible"
          multiline
          rowsMax="50"
          fullWidth={true}
          value={classHtmlContent}
          margin="normal"
          variant="outlined"
        />
      );
    });
    return {
      tabs,
      tabContainers
    }
  }

  handleDownloadFile = () => {
    /*
    let className = Object.keys(this.props.generatedClassesList)[this.state.value];
    let classContent = this.props.generatedClassesList[className];
    download(classContent, `${className}.java`, "text/plain");
    */
    this.setState({
      downloadDialogState: true
    })
  }
  /*
  generateProjectRequest = (className, classContent, lineConfig) => {
    Axios.post(
      process.env.REACT_APP_BACKEND_URL_PROJECT_GENERATE,
      {
        className,
        classContent,
        lineConfig
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    .then(res => {
      if (res.data === true) {
        this.setState({
          deployProjectStages: {
            ...this.state.deployProjectStages,
            generateProjectIcon: "valid"
          }
        }, () => {
          this.deployProjectRequest();
        })
      } else {
        this.setState({
          deployProjectStages: {
            ...this.state.deployProjectStages,
            generateProjectIcon: "invalid"
          }
        })
      }
    })
    .catch(err => {
       
    })
  }

  deployProjectRequest = () => {
    Axios.get(
      process.env.REACT_APP_BACKEND_URL_PROJECT_DEPLOY
    ).then(res => {
      if (res.data === true ) {
        this.setState({
          deployProjectStages: {
            ...this.state.deployProjectStages,
            deployProjectIcon: "valid",
            projectWebhookIcon: "valid"
          }
        })
      } else {
        this.setState({
          deployProjectStages: {
            ...this.state.deployProjectStages,
            deployProjectIcon: "invalid",
            projectWebhookIcon: "invalid"
          }
        })
      }
    }).catch(err => {
      throw err;
    })
  }
  */
  handleDeployProject = () => {
    this.changeDeployDialogState(true);
    // this.connectWebSocket();
    /*
    let className = Object.keys(this.props.generatedClassesList)[this.state.value];
    let classContent = this.props.generatedClassesList[className];
    let lineConfig = Store.getState().appSettings.lineChatBotConfig;
    
    this.generateProjectRequest(className, classContent, lineConfig);
    */
  }

  render() {
    const { value } = this.state;
    const { tabs, tabContainers } = this.generateClassesTabs();
    // const { classes } = this.props;
    return (
      <div className="GeneratedChatBotClassesDialog">
        <Dialog
          open={this.props.dialogSwitchState}
          onClose={this.handleDialogClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullScreen={true}
        >
          <DialogTitle id="alert-dialog-title">{"Generated ChatBot Classes"}</DialogTitle>
          <DialogContent>
            
          <AppBar position="static">
          <Tabs value={value} onChange={this.handleTabChange}>
            { tabs }
          </Tabs>
        </AppBar>
        <SwipeableViews
          index={this.state.value}
          onChangeIndex={this.handleChangeIndex}
        >
          { tabContainers }
        </SwipeableViews>

          </DialogContent>
          <DialogActions>
          {/* <Button onClick={this.handleDeployProject} color="primary">
              Deploy
            </Button>
          */}
            <Button onClick={this.handleDownloadFile} color="primary">
              Download
            </Button>
            <Button onClick={this.handleDialogClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
        
        <DeployChatBotDialog 
          dialogSwitchState = {this.state.deployDialogState}
          changeDialogSwitchState = {this.changeDeployDialogState}
          getChatBotClassConfig = {this.getChatBotClassConfig}
        />

        <DownloadChatBotDialog
          dialogSwitchState = {this.state.downloadDialogState}
          changeDialogSwitchState = {this.changeDownloadDialogState}
          getChatBotClassConfig = {this.getChatBotClassConfig}
        />

      </div>
    );
  }
}

/*
GeneratedChatBotClassesDialog.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GeneratedChatBotClassesDialog);
*/

export default GeneratedChatBotClassesDialog;