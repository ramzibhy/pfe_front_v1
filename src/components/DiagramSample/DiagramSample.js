import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as go from 'gojs';
import './DiagramSample.css';

import BotControllerClassNode from "../../nodes/botControllerClassNode";
import DialogflowintentNode from "../../nodes/dialogflowIntentNode";
import TemplateMessageNode from "../../nodes/templateMessageNode";
import CarouselTemplateNode from "../../nodes/carouselTemplateNode";
import CarouselColumnNode from "../../nodes/carouselColumnNode";
import ButtonTemplateNode from "../../nodes/buttonsTemplateNode";
import ConfirmTemplateNode from "../../nodes/confirmTemplateNode";
import TextMessageNode from "../../nodes/textMessageNode";
import MessageActionNode from "../../nodes/messageActionNode";
import UriActionNode from "../../nodes/uriActionNode";
import PostBackActionNode from "../../nodes/postBackActionNode"; 

/**
 * The Component that contain the model for the desired preview diagram.
 *
 * @class DiagramSample
 * @extends {Component}
 */
class DiagramSample extends Component {

  constructor(props) {
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  componentDidMount() {
    const $ = go.GraphObject.make;
    const myDiagram = $(go.Diagram, "myDiagramDiv", 
      {
      "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
        initialContentAlignment: go.Spot.Center,
      }
    );
    myDiagram.toolManager.draggingTool.isEnabled = false;
    myDiagram.toolManager.linkingTool.isEnabled = false;
    myDiagram.toolManager.contextMenuTool.isEnabled = false;
    myDiagram.toolManager.textEditingTool.isEnabled = false;
    
    myDiagram.nodeTemplateMap.add("BotControllerClassNode", BotControllerClassNode())
    myDiagram.nodeTemplateMap.add("DialogflowintentNode",DialogflowintentNode());
    myDiagram.nodeTemplateMap.add("TemplateMessageNode", TemplateMessageNode());
    myDiagram.nodeTemplateMap.add("CarouselTemplateNode", CarouselTemplateNode());
    myDiagram.nodeTemplateMap.add("CarouselColumnNode", CarouselColumnNode(this));
    myDiagram.nodeTemplateMap.add("ButtonTemplateNode", ButtonTemplateNode(this));
    myDiagram.nodeTemplateMap.add("ConfirmTemplateNode", ConfirmTemplateNode());
    myDiagram.nodeTemplateMap.add("TextMessageNode", TextMessageNode());
    myDiagram.nodeTemplateMap.add("MessageActionNode", MessageActionNode());
    myDiagram.nodeTemplateMap.add("UriActionNode", UriActionNode());
    myDiagram.nodeTemplateMap.add("PostBackActionNode", PostBackActionNode());
    
    myDiagram.model = go.Model.fromJson(this.props.diagramModelList[this.props.selectedDiagram]);
  }

  render() {
    return (
      <div className="DiagramSample">
        <div id="myDiagramDiv" className='myDiagramSample'></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    diagramModelList: state.diagramModelList,
    selectedDiagram: state.selectedDiagram,
  }
}

export default connect(mapStateToProps)(DiagramSample);
