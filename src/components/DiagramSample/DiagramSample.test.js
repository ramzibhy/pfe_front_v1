import React from 'react';
import ReactDOM from 'react-dom';
import DiagramSample from './DiagramSample';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DiagramSample />, div);
  ReactDOM.unmountComponentAtNode(div);
});
