import React, { Component } from 'react';
import ImageGallery from 'react-image-gallery';
import './Home.css';

/**
 * The Home component.
 *
 * @class Home
 * @extends {Component}
 */
class Home extends Component {
  render() {

    const images = [
      {
        original: '../../../images/tutorial1.png',
        thumbnail: '../../../images/tutorial1.png',
      },
      {
        original: '../../../images/tutorial2.png',
        thumbnail: '../../../images/tutorial2.png',
      },
      {
        original: '../../../images/tutorial3.png',
        thumbnail: '../../../images/tutorial3.png',
      },
      {
        original: '../../../images/tutorial4.png',
        thumbnail: '../../../images/tutorial4.png',
      }
    ]

    return (
      <div className="Home">
        <ImageGallery
          showFullscreenButton={false}
          showPlayButton={false}
          showThumbnails={false}
          showBullets={true}
          items={images}
        />
      </div>
    );
  }
}

export default Home;
