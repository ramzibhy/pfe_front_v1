import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import PropTypes from 'prop-types';
import Store from '../../store/store';
import DiagramModelPreview from '../DiagramModelPreview/DiagramModelPreview';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import DeveloperBoard from '@material-ui/icons/DeveloperBoard';
import Button from '@material-ui/core/Button';
import { Link, withRouter } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import './ChatBotLoader.css';

const styles = {
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: '#e8f0ff',
    borderRadius: 10,
    margin: '0px 20px',
  },
  customButton: {
    background: '#2196F3',
    '&:hover': {
      backgroundColor: '#2c79f4'
    },
    margin: '0px 10px',
  },
  newDiagramButton: {

    margin: '20px 20px',
  }
};

/**
 * The component that load all the stocked diagram models and and render it as a list.
 *
 * @class ChatBotLoader
 * @extends {Component}
 */
class ChatBotLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      diagramModelItemList: {},
    }
  }

  diagramModelTemplate = (diagramName, classes) => {
    return (
      <ListItem key={diagramName}>
        <ListItemAvatar>
          <Avatar>
            <DeveloperBoard />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={ diagramName }
        />
        <Button
          variant="contained"
          color="primary"
          className={classes.customButton}
          onClick={() => this.previewSelectedDiagram(true, diagramName)}
        >
          Preview
        </Button>
        <Button
          /*
          component={Link}
          to={
            {
              pathname: "/diagramdragdrop",
              state: {
                newDiagram: false,
              }
            }
          }
          */
          variant="contained"
          color="primary"
          className={classes.customButton}
          onClick={() => this.editSelectedDiagram(diagramName)}
        >
          Edit
        </Button>
        <IconButton
          aria-label="Delete"
          onClick= {() => this.deleteSelectedDiagram(diagramName)}  
        >
          <DeleteIcon />
        </IconButton>
      </ListItem>
    );
  }

  generateDiagramModelItemList = (diagramModelList, classes) => {
    let ItemList = [];
    Object.keys(diagramModelList).forEach((diagramName) => {
      ItemList.push(this.diagramModelTemplate(diagramName, classes));
    })
    return {
      diagramModelItemList: ItemList,
      listInfoText: (ItemList.length !== 0)? "" : "There was no diagram model stocked in the database",
    };
  }

  previewSelectedDiagram = (status, diagramName) => {
    Store.dispatch({
      type: "setPreviewDiagramDialog",
      previewDiagramDialog: status,
    });
    Store.dispatch({
      type: "setSelectedDiagram",
      selectedDiagram: diagramName,
    })
  }

  editSelectedDiagram = (diagramName) => {
    const appStore = Store.getState();
    const appComponent = appStore.appComponent;
    const appSettings = appStore.appSettings;
    if ( appSettings.dialogFlowConfig.status !== "valid" ) {
      appComponent.props.enqueueSnackbar('Please set a correct dialogflow configuration.' , {variant: 'error'});
    } else if ( appSettings.lineChatBotConfig.status !== "valid" ) {
      appComponent.props.enqueueSnackbar('Please set a correct line chatbot configuration.' , {variant: 'error'});
    } else {
      Store.dispatch({
        type: "setSelectedDiagram",
        selectedDiagram: diagramName,
      })
      this.props.history.push({
        pathname: "/diagramdragdrop",
        state: {
          newDiagram: false,
        }
      })
    }
  }

  deleteSelectedDiagram = (diagramName) => {
    const appStore = Store.getState();
    axios.post(
      `${process.env.REACT_APP_BACKEND_WEBHOOK}/line/delete_model`,
      { diagramName }
    ).then((response) => {
      if(response.status === 200) {
        Store.dispatch({
          type: "removeFromDiagramModelList",
          diagramName
        });
        appStore.appComponent.props.enqueueSnackbar('Diagram model was successfully deleted.' , {variant: 'success'});
      }
    }).catch((err)=> {
      appStore.appComponent.props.enqueueSnackbar('There was an error while deleting the diagram model.' , {variant: 'error'});
      throw err;
    });
  }

  createNewDiagram = () => {
    const appStore = Store.getState();
    const appComponent = appStore.appComponent;
    const appSettings = appStore.appSettings;
    if ( appSettings.dialogFlowConfig.status !== "valid" ) {
      appComponent.props.enqueueSnackbar('Please set a correct dialogflow configuration.' , {variant: 'error'});
    } else if ( appSettings.lineChatBotConfig.status !== "valid" ) {
      appComponent.props.enqueueSnackbar('Please set a correct line chatbot configuration.' , {variant: 'error'});
    } else {
    this.props.history.push({
      pathname: "/diagramdragdrop",
      state: {
        newDiagram: true,
        }
      })
    }
  }
  
  render() {
     
    const { classes } = this.props;
    let { diagramModelItemList, listInfoText } = this.generateDiagramModelItemList(this.props.diagramModelList, classes);
    //  
    return (
      <div>
        <Button
          //component={Link}
          variant="outlined"
          className={classes.newDiagramButton}
          onClick={this.createNewDiagram}
          /*
          to={
            {
              pathname: "/diagramdragdrop",
              state: {
                newDiagram: true,
              }
            }
          }
          */
        >
          New Diagram
        </Button>
        <div className={classes.demo}>
        
          <DiagramModelPreview />
          {
            //<button onClick={this.showProps}> test </button>
          }
          <List>
            <div>{listInfoText}</div>
            {
              diagramModelItemList
            }
          </List>
          
        </div>
      </div>
    );
  }
}

ChatBotLoader.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    diagramModelList: state.diagramModelList,
  }
}

const styledComponent = withStyles(styles)(ChatBotLoader);
const routedComponent = withRouter(styledComponent);
export default connect(mapStateToProps)(routedComponent);
