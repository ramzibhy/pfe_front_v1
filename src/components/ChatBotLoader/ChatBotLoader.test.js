import React from 'react';
import ReactDOM from 'react-dom';
import ChatBotLoader from './ChatBotLoader';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ChatBotLoader />, div);
  ReactDOM.unmountComponentAtNode(div);
});
