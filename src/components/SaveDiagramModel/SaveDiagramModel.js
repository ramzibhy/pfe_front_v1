import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Store from '../../store/store';
import axios from 'axios';
import './SaveDiagramModel.css';

const styles = {
  saveButton: {
    margin: '20px 10px',
    height: '49px',
  },
  textField: {
    marginTop: '20px',
    marginLeft: '10px',
  }
};

/**
 * Saves the diagram model edited or created in the database and in application store.
 * @param {String} diagramName The diagram model name. 
 * @param {JSON} diagramModelJson The diagram model json object.
 */
const saveDiagram = (diagramName, diagramModelJson) => {
  const appStore = Store.getState();
  let data = {}
  data[diagramName] = diagramModelJson;
  /*
  Store.dispatch({
    type: "addToSavedDiagramModelList",
    diagramName,diagramModelJson
  });
  */
  axios.post(
    `${process.env.REACT_APP_BACKEND_WEBHOOK}/line/save_model`,
    data,
  ).then((response) => {
    if (response.status === 200) {
      Store.dispatch({
        type: "addToDiagramModelList",
        diagramName,diagramModelJson
      });
      appStore.appComponent.props.enqueueSnackbar('Diagram model was successfully saved.', {variant: 'success'})
    }
  }).catch((err) => {
    appStore.appComponent.props.enqueueSnackbar('There was an error in the saving of the diagram model.', {variant: 'error'})
    throw err;
  })
  
} 

/**
 * The Component that the that enable the user 
 * to save an edited or a new created diagram model.
 *
 * @class SaveDiagramModel
 * @extends {Component}
 */
class SaveDiagramModel extends Component {

  constructor(props) {
    super(props);
    // this.setUnmountPageListener();
    this.state = {
      saveFileName: (this.props.newDiagram)? "":this.props.diagramName ,
    }
  }

  /*
  setUnmountPageListener = () => {
    // Send the new diagram Models to the backend in order to be saved in the database
    window.addEventListener("beforeunload", (ev) => 
    {  
        axios.post(
            'http://localhost:8080/save',
            this.props.savedDiagramModelList,
        )
    });
  }
  */

  handleChange = saveFileName => event => {
    this.setState({
      [saveFileName]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;
     
    return (
      <div className="SaveDiagramModel">
        <TextField
          label="Name"
          className={classes.textField}
          value={this.state.saveFileName}
          onChange={this.handleChange('saveFileName')}
          variant="outlined"
          margin="dense"
        />
        <Button
          variant="outlined"
          className={classes.saveButton}
          onClick={() => saveDiagram(this.state.saveFileName,this.props.getDiagramModel())}
        >
          Save Diagram
        </Button>
      </div>
    );
  }
}

SaveDiagramModel.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SaveDiagramModel);
