import React from 'react';
import ReactDOM from 'react-dom';
import SaveDiagramModel from './SaveDiagramModel';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SaveDiagramModel />, div);
  ReactDOM.unmountComponentAtNode(div);
});
