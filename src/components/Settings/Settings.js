import request from "request";
import React, { Component } from 'react';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import Store from '../../store/store';
import Axios from 'axios';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ValidIcon from '@material-ui/icons/CheckCircleOutline';
import InvalidIcon from '@material-ui/icons/HighlightOff';
import UnknownIcon from '@material-ui/icons/HelpOutline';

import { withStyles } from '@material-ui/core/styles';
import './Settings.css';

const styles = {
  container: {
    backgroundColor: '#e8f0ff',
    borderRadius: 10,
    margin: '20px 20px',
  },
  customText: {
    marginLeft: '20px',
    marginTop: '5px',
    color: '#353535',
    
  },
  customTextField: {
    marginTop: '-5px',
    marginLeft: '20px',
    width: '95%',
    marginBottom: '20px',
  },
  customButton: {
    width: "100px",
    marginTop: '-10px',
    marginRight: '10px',
    float: 'right',
  },
  chatBotTypeText: {
    display: "inline",
    marginLeft: "20px",
  },
  chatBotTypeSelectList: {
    width: '200px',
    marginLeft: '20px',
  }
};

const verifyDialogflowConfig = (dialogflowAgentToken) => {
  const config = {
    headers: {
      'Authorization': 'Bearer ' + dialogflowAgentToken 
    }
  }
  return Axios.get(
    `${process.env.REACT_APP_DIALOGFLOW_INTENTS_WEBHOOK}`,
    config
  )
  .then(res => {
    return res.status;
  })
}

const verifyLineChatBotConfig = (lineChannelId, lineChannelSecret) => {
  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }
  const requestBody = {
    grant_type: 'client_credentials',
    client_id: lineChannelId,
    client_secret: lineChannelSecret
  }
  return Axios.post(
    `${process.env.REACT_APP_LINE_TOKEN_VERIFICATION_URL}`,
    requestBody,
    config
  )
  .then(res => {
    if (res === 200) {
      let lineChannelAccessToken = res.data["access_token"];
       
      // return verifyLineChannelAccessToken(lineChannelAccessToken);
    }
  })
  .catch (err => {
     
  })
}

const verifyLineChatBotConfigProxy = (lineChannelId, lineChannelSecret) => {
  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }
  const requestBody = {
    grant_type: 'client_credentials',
    client_id: lineChannelId,
    client_secret: lineChannelSecret
  }
  const proxyBody = {
    url: `${process.env.REACT_APP_LINE_TOKEN_VERIFICATION_URL}`,
    config,
    requestBody
  }

  return Axios.post(
    `${process.env.REACT_APP_BACKEND_CORS_SERVICE}/post`,
    proxyBody,
    {
      headers: {
        'Content-Type': 'application/json'
      }
    }
  )
  .then(res => {
    if (res.status === 200) {
      let lineChannelAccessToken = res.data["access_token"];
      if (lineChannelAccessToken) {
        return verifyLineChannelAccessTokenProxy(lineChannelAccessToken);
      } else {
        throw "Invalid ChannelID or ChannelSecret."
      }
    }
  })
  .catch (err => {
    throw err;
  })
}

const verifyLineChannelAccessToken = (lineChannelAccessToken) => {
   
  const config = {
    headers: {
      'Authorization': 'Bearer ' + lineChannelAccessToken
    }
  }
  return Axios.get(
    `${process.env.REACT_APP_LINE_CHANNEL_VERIFICATION_URL}`,
    config
  )
  .then(res => {
    console.log(res.data)
    return {
      status: res.status,
      lineChannelAccessToken
    }
  })
  .catch (err => {
    throw err;
  })
}

const verifyLineChannelAccessTokenProxy = (lineChannelAccessToken) => {
  const config = {
    headers: {
      'Authorization': 'Bearer ' + lineChannelAccessToken
    }
  }
  const proxyBody = {
    url: `${process.env.REACT_APP_LINE_CHANNEL_VERIFICATION_URL}`,
    config

  }
  return Axios.post(
    `${process.env.REACT_APP_BACKEND_CORS_SERVICE}/get`,
    proxyBody,
    {
      headers: {
        'Content-Type': 'application/json'
      }
    }
  )
  .then(res => {
    return {
      status: res.status,
      lineChannelAccessToken
    }
  })
  .catch (err => {
    throw err;
  })
}

const configurationStatusIcon = (configurationStatus) => {
  const defautlStyle = {
    display: "inline",
    float: "right",
    marginTop: "-5px",
    marginRight: "20px"

  }
  switch ( configurationStatus ) {
    case "valid":
      return <ValidIcon style={{
        ...defautlStyle,
        color: "#32CD32"
      }} />
    case "invalid":
      return <InvalidIcon style={{
        ...defautlStyle,
        color: "#FF0000"
      }} />
    default:
      return <UnknownIcon style={{
        ...defautlStyle,
        color: "#000000"
      }} />
  }
}

/**
 * The Settings component.
 *
 * @class Settings
 * @extends {Component}
 */
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogflowAgentToken: this.props.appSettings.dialogFlowConfig.dialflowAgentToken,
      
      lineChannelId: this.props.appSettings.lineChatBotConfig.lineChannelId,
      lineChannelSecret: this.props.appSettings.lineChatBotConfig.lineChannelSecret,
      lineChannelAccessToken: this.props.appSettings.lineChatBotConfig.lineChannelAccessToken,
      chatBotType: "Line",
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.appSettings !== prevProps.appSettings) {
      this.setState({
        dialogflowAgentToken: this.props.appSettings.dialogFlowConfig.dialflowAgentToken,
        lineChannelId: this.props.appSettings.lineChatBotConfig.lineChannelId,
        lineChannelSecret: this.props.appSettings.lineChatBotConfig.lineChannelSecret,
        lineChannelAccessToken: this.props.appSettings.lineChatBotConfig.lineChannelAccessToken,
      })
    }
  }

  handleTextFieldChange = name => event => {
    this.setState({
      [name]: event.target.value
    })
  }

  handleChatBotTypeChange = event => {
    this.setState({
      chatBotType: event.target.value
    })
  }

  applyDialogflowConfig = () => {
    let configStatus = "unknown";
    const appComponent = Store.getState().appComponent;
    appComponent.props.enqueueSnackbar('Verifying the dialogflow agent token ...');
    verifyDialogflowConfig(this.state.dialogflowAgentToken)
    .then(res => {
      if (res === 200) {
        appComponent.props.enqueueSnackbar('Valid dialogflow agent token.', {variant: 'success'});
        configStatus = "valid";

        localStorage.setItem('dialogFlowConfig', JSON.stringify({
          status: configStatus,
          dialflowAgentToken: this.state.dialogflowAgentToken
        }))
        
      } else {
        appComponent.props.enqueueSnackbar('Invalid dialogflow agent token.', {variant: 'error'});
        configStatus = "invalid";
      }
    })
    .catch(err => {
      appComponent.props.enqueueSnackbar('Invalid dialogflow agent token.', {variant: 'error'});
      configStatus = "invalid";
    })
    .then(() => {
      Store.dispatch({
        type: "updateDialogFlowConfig",
        dialogFlowConfig: {
          dialflowAgentToken: this.state.dialogflowAgentToken,
          status: configStatus
        }
      })
    })
  }

  applyLineChatBotConfig = () => {
    let configStatus = "unknown";
    let lineChannelAccessToken = "";
    const appComponent = Store.getState().appComponent;
    appComponent.props.enqueueSnackbar('Verifying the line chatbot config ...');
    verifyLineChatBotConfigProxy(this.state.lineChannelId, this.state.lineChannelSecret)
    .then(res => {
       
      if (res.status === 200) {
        configStatus = "valid";
        lineChannelAccessToken = res.lineChannelAccessToken;
        appComponent.props.enqueueSnackbar('Valid Line ChatBot Config.', {variant: 'success'});
        
        localStorage.setItem('lineChatBotConfig', JSON.stringify({
          status: configStatus,
          lineChannelId: this.state.lineChannelId,
          lineChannelSecret: this.state.lineChannelSecret,
          lineChannelAccessToken: res.lineChannelAccessToken
        }));
        
      } else {
        configStatus = "invalid";
        throw "Invalid Line ChatBot Channel Token";
      }
    })
    .catch(err => {
      appComponent.props.enqueueSnackbar(err, {variant: 'error'});
      configStatus = "invalid";
    })
    .then(() => {
      Store.dispatch({
        type: "updateLineChatBotConfig",
        lineChatBotConfig: {
          lineChannelId: this.state.lineChannelId,
          lineChannelSecret: this.state.lineChannelSecret,
          lineChannelAccessToken,
          status: configStatus
        }
      })
    })
  }

  lineChatBotConfigTemplate = () => {
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.container}>
          <br></br>
          <h3 className={classes.customText}>
            Line ChatBot Configuration:
          </h3>
          <TextField
            id="outlined-name"
            label="Line Channel ID"
            multiline
            className={classes.customTextField}
            value={this.state.lineChannelId}
            onChange={this.handleTextFieldChange("lineChannelId")}
            margin="normal"
          />
          <br></br>
          <TextField
            id="outlined-name"
            label="Line Channel Secret"
            multiline
            className={classes.customTextField}
            value={this.state.lineChannelSecret}
            onChange={this.handleTextFieldChange("lineChannelSecret")}
            margin="normal"
          />
          {
            /*
            <br></br>
            <TextField
              id="outlined-name"
              label="Line Channel Access Token"
              multiline
              className={classes.customTextField}
              value={this.state.lineChannelAccessToken}
              onChange={this.handleTextFieldChange("lineChannelAccessToken")}
              margin="normal"
            />
            */
          }
        </div>
        {
          configurationStatusIcon(this.props.appSettings.lineChatBotConfig.status)
        }
        <Button
          variant="outlined"
          className={classes.customButton}
          onClick={this.applyLineChatBotConfig}
        >
          Apply
        </Button>
      </div>
    )
  }

  renderChatBotConfig = () => {
    switch(this.state.chatBotType) {
      case "Line":
        return this.lineChatBotConfigTemplate();
      
      default:
        return <div style={{
          marginLeft: "20px",
          marginTop: "20px"
        }}>
          Not Implemented Yet.
        </div>
    }
      
  }

  render() {
     
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.container}>
          <br></br>
          <h3 className={classes.customText}>
            Dialogflow Configuration:
          </h3>
          <TextField
            id="outlined-name"
            label="Dialogflow Agent Token"
            multiline
            className={classes.customTextField}
            value={this.state.dialogflowAgentToken}
            onChange={this.handleTextFieldChange("dialogflowAgentToken")}
            margin="normal"
          />
        </div>
        {
          configurationStatusIcon(this.props.appSettings.dialogFlowConfig.status)
        }
        <Button
          variant="outlined"
          className={classes.customButton}
          onClick={this.applyDialogflowConfig}
        >
          Apply
        </Button>
        <br></br>
        <hr style={{
          margin: '20px 20px 10px 20px'
        }}></hr>
        <div>
          <h3 className={classes.chatBotTypeText}>
            ChatBot Platform:
          </h3>
        <Select
            className={classes.chatBotTypeSelectList}
            value={this.state.chatBotType}
            onChange={this.handleChatBotTypeChange}
            name="ChatBot Type"
          >
            <MenuItem value="Line">
              <em>Line</em>
            </MenuItem>
            <MenuItem value="Messanger">Messanger</MenuItem>
          </Select>
        </div>
        
        {
          this.renderChatBotConfig()
        }
      </div>
    );
  }
}

Settings.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    appSettings: state.appSettings,
  }
}

const styledComponent = withStyles(styles)(Settings);
export default connect(mapStateToProps)(styledComponent);
