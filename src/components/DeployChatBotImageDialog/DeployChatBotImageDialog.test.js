import React from 'react';
import ReactDOM from 'react-dom';
import DeployChatBotImageDialog from './DeployChatBotImageDialog';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DeployChatBotImageDialog />, div);
  ReactDOM.unmountComponentAtNode(div);
});
