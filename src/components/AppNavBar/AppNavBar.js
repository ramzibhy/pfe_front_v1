import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import InvertColors from '@material-ui/icons/InvertColors';
import Store from '../../store/store';
import { Link } from 'react-router-dom';
import './AppNavBar.css';

const styles = {
  navBar: {
    background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
    color: 'white',
    padding: '0 30px',
  },
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  invertThemeButton: {
    color: '#FFFFFF',
  }
};


/**
 * The navigation bar component of our app. 
 * It contains all the buttons that navigate to the desired components.
 * 
 * @class AppNavBar
 * @extends {Component}
 */
class AppNavBar extends Component {
  constructor(props) {
    super(props);
  }

  handleInvertTheme = () => {
    /*
    let appStore = Store.getState();
     
    Store.dispatch({
      type: "setAppTheme",
      appTheme: "light"
    });
    */
    
  }

  render() {
    const { classes } = this.props;
    return (
      <AppBar position="static">
        <Toolbar className={classes.navBar}>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            BotGen
          </Typography>
          <Button color="inherit" component={Link} to="/">Home</Button>
          <Button color="inherit" component={Link} to="/chatbot">ChatBot Loader</Button>
          <Button color="inherit" component={Link} to="/chatbot_reviewer">ChatBot Reviewer</Button>
          <Button color="inherit" component={Link} to="/settings">Settings</Button>
          <IconButton
            className={classes.invertThemeButton}
            aria-label="Invert theme"
            onClick={this.handleInvertTheme}
          >
            <InvertColors />
          </IconButton>
        </Toolbar>
      </AppBar>
    );
  }
}

AppNavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppNavBar);
