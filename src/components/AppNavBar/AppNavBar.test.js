import React from 'react';
import ReactDOM from 'react-dom';
import AppNavBar from './AppNavBar';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AppNavBar />, div);
  ReactDOM.unmountComponentAtNode(div);
});
