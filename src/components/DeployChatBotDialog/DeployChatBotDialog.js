import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';

import ValidIcon from '@material-ui/icons/CheckCircleOutline';
import InvalidIcon from '@material-ui/icons/HighlightOff';
import UnknownIcon from '@material-ui/icons/HelpOutline';

import * as SockJS from "sockjs-client";
import { Stomp } from "@stomp/stompjs";

import './DeployChatBotDialog.css';

const styles = {
  loadingCircle: {
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "50px",
    display: "block"
  },
  generateProjectDiv: {
    marginRight: "auto",
    display: "inline"
  },
  deployProjectDiv: {
    display: "inline"
  },
  projectWebhookDiv: {
    marginLeft: "auto",
    display: "inline"
  },
  logBox: {
    marginTop: "20px",
    display: "block"
  },
  dialogBox: {
    width: "500px"
  }
}

const configurationStatusIcon = (configurationStatus) => {
  switch ( configurationStatus ) {
    case "valid":
      return <ValidIcon style={{
        color: "#32CD32"
      }} />
    case "invalid":
      return <InvalidIcon style={{
        color: "#FF0000"
      }} />
    default:
      return <UnknownIcon style={{
        color: "#000000"
      }} />
  }
}

/**
 * The DeployChatBotDialog component.
 *
 * @class DeployChatBotDialog
 * @extends {Component}
 */
class DeployChatBotDialog extends Component {

  deployStages = {
    generate: "generate",
    deploy: "deploy"
  }

  deployStates = {
    done: "done",
    progress: "progress",
    error: "error"
  }

  state = {
    stompClient: null,
    generateProjectIcon: "unknown",
    deployProjectIcon: "unknown",
    deployProjectLog: "",
  }

  connectWebSocket = () => {
    let classConfig = this.props.getChatBotClassConfig();
    let socket = new SockJS(process.env.REACT_APP_BACKEND_URL_WEB_SOCKET);
     
    let stompClient = Stomp.over(socket);
    stompClient.connect({}, (frame) => {
       
      stompClient.subscribe('/user/topic/messages', (webSocketReply) => {
        this.handleWebSocketReply(JSON.parse(webSocketReply.body));
         
      });
      this.setState({
        stompClient
      }, () => {
        stompClient.send(
          process.env.REACT_APP_WEB_SOCKET_PROJECT_DEPLOY,
          {}, 
          JSON.stringify(classConfig)
        );
      });
      
      /*
      setTimeout(()=> {
        
      }, 3000)
      */
    })
  }

  disconnectWebSocket = () => {
    if(this.state.stompClient != null) {
      this.state.stompClient.disconnect();
    }
     
  }

  handleWebSocketReply = (webSocketReply) => {
     
    switch (webSocketReply.currentState) {
      case this.deployStates.done:
        this.updateDeployProjectInfo(
          webSocketReply.currentStage,
          webSocketReply.log,
          "valid"
        );
        break;
      case this.deployStates.progress:
        this.updateDeployProjectLog(webSocketReply.log);
        break;
      case this.deployStates.error:
        this.updateDeployProjectInfo(
          webSocketReply.currentStage,
          webSocketReply.log,
          "invalid"
        );
        throw "Deployement Failed"
        break;
      default:
        throw "Invalid deploy project state";
    }
  }

  //update the stages icon and the logs
  updateDeployProjectInfo = (currentStage, newLog, iconState) => {
    switch (currentStage) {
      case this.deployStages.generate:
        this.setState({
          generateProjectIcon: iconState, 
        })
        this.updateDeployProjectLog(newLog);
        break;
      case this.deployStages.deploy:
        this.setState({
          deployProjectIcon: iconState,
        })
        this.updateDeployProjectLog(newLog);
        break;
      default:
        throw "Invalid deploy project stage";
    }
  }

  updateDeployProjectLog = (newLog) => {
    this.setState({
      deployProjectLog: (this.state.deployProjectLog!=="")? `${this.state.deployProjectLog}\n${newLog}` : newLog,
    })
  }

  handleDeployProjectSuccess = () => {
    
  }

  handleDialogOpen = () => {
    this.connectWebSocket();
  }

  handleDialogClose = () => {
    this.disconnectWebSocket();
    this.props.changeDialogSwitchState(false);
    this.setState({
      generateProjectIcon: "unknown",
      deployProjectIcon: "unknown",
      deployProjectLog: ""
    })
  }

  render() {
    const { classes } = this.props;
    return (
      <div className="DeployChatBotDialog">
        <Dialog
          open={this.props.dialogSwitchState}
          onEntered={this.handleDialogOpen}
          onClose={this.handleDialogClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth='xl'
        >
          <DialogTitle id="alert-dialog-title">{"Deploying Project ..."}</DialogTitle>
          <DialogContent>
            <div className={classes.generateProjectDiv}>
              {
                configurationStatusIcon(this.state.generateProjectIcon)
              }
              Generate
            </div>
            <div style={{
              display: "inline"
            }}> ----------------------------> </div>
            <div className={classes.deployProjectDiv}>
              {
                configurationStatusIcon(this.state.deployProjectIcon)
              }
              Deploy
            </div>

            {/*
              <div style={{
              display: "inline"
              }}> ----------------------------> </div>
              <div className={classes.projectWebhookDiv}>
                {
                  configurationStatusIcon(this.props.deployProjectStages.projectWebhookIcon)
                }
                WebHook
              </div>
            */}

            <TextField
              id="outlined-multiline-static"
              className={classes.logBox}
              fullWidth={true}
              multiline
              rows="10"
              value={this.state.deployProjectLog}
              variant="outlined"
            />
            
            {
              /* <CircularProgress className={classes.loadingCircle}/> */
            }
            
            {/*
              <DialogContentText id="alert-dialog-description">
                Let Google help apps determine location. This means sending anonymous location data to
                Google, even when no apps are running.
              </DialogContentText>
            */}
          
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

DeployChatBotDialog.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DeployChatBotDialog);
