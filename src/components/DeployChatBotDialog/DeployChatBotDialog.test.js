import React from 'react';
import ReactDOM from 'react-dom';
import DeployChatBotDialog from './DeployChatBotDialog';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DeployChatBotDialog />, div);
  ReactDOM.unmountComponentAtNode(div);
});
