import React from 'react';
import ReactDOM from 'react-dom';
import ChatBotReviewer from './ChatBotReviewer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ChatBotReviewer />, div);
  ReactDOM.unmountComponentAtNode(div);
});
