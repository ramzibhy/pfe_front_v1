import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Axios from 'axios';
import Store from '../../store/store';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import DeveloperBoard from '@material-ui/icons/DeveloperBoard';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';

import * as SockJS from "sockjs-client";
import { Stomp } from "@stomp/stompjs";

import './ChatBotReviewer.css';

const styles = {
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: '#e8f0ff',
    borderRadius: 10,
    margin: '30px 20px',
  },
  customButton: {
    background: '#2196F3',
    '&:hover': {
      backgroundColor: '#2c79f4'
    },
    margin: '0px 10px',
  },
  newDiagramButton: {

    margin: '20px 20px',
  },
  progress: {
    margin: '20px 150px',
  },
};

/**
 * The ChatBotReviewer component.
 *
 * @class ChatBotReviewer
 * @extends {Component}
 */
class ChatBotReviewer extends Component {
  constructor (props) {
    super(props);
    const AppStore = Store.getState();
    this.state = {
      stompClient: null,

      generatedProjectList: [],
      appComponent: AppStore.appComponent,
      downloadDockerImageDialog: false,
      downloadDockerImageLoading: false,
      downloadDockerImageStage: "",

      deployGeneratedProjectDialog: false,
      deployGeneratedProjectContent: false,
      deployDockerImageStage: "",
      deployGeneratedProjectWebhook: "",

    }
    this.getGeneratedProjectList();
  }

  //---------------------------- Web Socket ---------------------------------------
  downloadImageWS = (generatedProjectId) => {
    let socket = new SockJS(process.env.REACT_APP_BACKEND_URL_WEB_SOCKET);
    let stompClient = Stomp.over(socket);
    stompClient.connect({}, (frame) => {
      console.log('Connected: ' + frame);
      stompClient.subscribe('/user/topic/messages', (webSocketReply) => {
        this.handleDownloadImageWebSocketReply(JSON.parse(webSocketReply.body));
      });
      this.setState({
        stompClient
      }, () => {
        stompClient.send(
          process.env.REACT_APP_WEB_SOCKET_DOCKER_IMAGE_DOWNLOAD,
          {}, 
          generatedProjectId
        )
      });
    })
  }

  handleDownloadImageWebSocketReply = (data) => {
    this.setState({
      downloadDockerImageStage: data.stageMessage,
    })
    if (data.stageMessage === "Done") {
      this.downloadFromUrl(data.replyMessage);
      this.setState({
        downloadDockerImageLoading: false,
        downloadDockerImageDialog: false,
      })
      this.disconnectWebSocket();
    }
  }

  deployImageWS = (generatedProjectId) => {
    let socket = new SockJS(process.env.REACT_APP_BACKEND_URL_WEB_SOCKET);
    let stompClient = Stomp.over(socket);
    stompClient.connect({}, (frame) => {
      console.log('Connected: ' + frame);
      stompClient.subscribe('/user/topic/messages', (webSocketReply) => {
        this.handleDeployImageWebSocketReplay(JSON.parse(webSocketReply.body));
      });
      this.setState({
        stompClient
      }, () => {
        stompClient.send(
          process.env.REACT_APP_WEB_SOCKET_DOCKER_IMAGE_DEPLOY,
          {}, 
          generatedProjectId
        )
      });
    })
  }

  handleDeployImageWebSocketReplay = (data) => {
    this.setState({
      deployDockerImageStage: data.stageMessage,
    })
    if (data.stageMessage === "Done") {
      this.setState({
        deployGeneratedProjectWebhook: data.replyMessage,
        deployGeneratedProjectContent: true
      });
      this.disconnectWebSocket();
    }
  }


  disconnectWebSocket = () => {
    if(this.state.stompClient != null) {
      this.state.stompClient.disconnect();
    }
    console.log("Disconnected");
  }

  //--------------------------------------------------------------------

  getGeneratedProjectList = () => {
    Axios.get(`${process.env.REACT_APP_BACKEND_WEBHOOK}/line/get_generated_projects`)
    .then((res) => {
      console.log(res.data)
      this.setState({
        generatedProjectList: res.data
      })
    })
  }

  getGeneratedProjectTemplate = (generatedProject, classes) => {
    return (
      <ListItem key={generatedProject.diagramModelName}>
        <ListItemAvatar>
          <Avatar>
            <DeveloperBoard />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={ generatedProject.diagramModelName }
        />
        <Button
          variant="contained"
          color="primary"
          className={classes.customButton}
          onClick={() => this.downloadMavenProject(generatedProject.projectUrl)}
        >
          Download Maven
        </Button>
        <Button
          variant="contained"
          color="primary"
          className={classes.customButton}
          onClick={() => this.dowloadDockerImage(generatedProject.id)}
        >
          Download Image
        </Button>
        <Button
          variant="contained"
          color="primary"
          className={classes.customButton}
          onClick={() => this.deployGeneratedProject(generatedProject.id)}
        >
          Deploy Chatbot
        </Button>
        <IconButton
          aria-label="Delete"
          onClick= {() => this.deleteGeneratedProject(generatedProject.id, generatedProject.projectPath)}  
        >
          <DeleteIcon />
        </IconButton>
      </ListItem>
    )
  }

  downloadFromUrl = (uri) => {
    var link = document.createElement("a");
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  downloadMavenProject = (projectUrl) => {
    this.downloadFromUrl(projectUrl);
  }

  dowloadDockerImage = (generatedProjectId) => {
    this.downloadImageWS(generatedProjectId);
    this.setState({
      downloadDockerImageDialog: true,
      downloadDockerImageLoading: true,
    });
    /*
    this.setState({
      downloadDockerImageDialog: true,
      downloadDockerImageLoading: true,
    }, () => {
      Axios({
        url: `${process.env.REACT_APP_BACKEND_WEBHOOK}/line/download_project_image`,
        data: {
          generatedProjectId,
        },
        method: 'post',
        config: {
          headers: {'Content-Type': 'application/json' }
        },
      })
      .then((res) => {
        this.setState({
          downloadDockerImageLoading: false,
        }, () => {
          this.downloadFromUrl(res.data);
          this.setState({
            downloadDockerImageDialog: false,
          })
        })        
      })
    })
    */
  }

  deleteGeneratedProject = (generatedProjectId, generatedProjectPath) => {
    Axios({
      url: `${process.env.REACT_APP_BACKEND_WEBHOOK}/line/delete_generated_project`,
      data: {
        id: generatedProjectId,
        projectPath: generatedProjectPath,
      },
      method: 'post',
      config: {
        headers: {'Content-Type': 'application/json' }
      },
    })
    .then((res) => {
      console.log(res.data);
      if (res.data) {
        this.state.appComponent.props.enqueueSnackbar('The project was successfully deleted' , {variant: 'success'});
        this.setState({
          generatedProjectList: this.state.generatedProjectList.filter((n) => n.id !== generatedProjectId)
        })
      } else {
        this.state.appComponent.props.enqueueSnackbar('There was an error while deleting the project' , {variant: 'error'});
      }
    })
  }

  deployGeneratedProject = (generatedProjectId) => {
    this.deployImageWS(generatedProjectId);
    this.setState({
      deployGeneratedProjectDialog: true,
    });
    /*
    this.setState({
        deployGeneratedProjectDialog: true,
    }, () => {
      Axios({
        url: `${process.env.REACT_APP_BACKEND_WEBHOOK}/line/deploy_project_image`,
        data: {
          generatedProjectId,
        },
        method: 'post',
        config: {
          headers: {'Content-Type': 'application/json' }
        },
      })
      .then((res) => {
        this.setState({
          deployGeneratedProjectWebhook: res.data["webHook"],
          deployGeneratedProjectContent: true
        })        
      })
    })
    */
  }

  getGeneratedProjectTemplateList = (generatedProjectList, classes) => {
    let generatedProjectTemplateList = [];
    generatedProjectList.forEach((generatedProject) => {
      generatedProjectTemplateList.push(this.getGeneratedProjectTemplate(generatedProject, classes));
    })
    return {
      generatedProjectTemplateList,
      listInfoText: (generatedProjectTemplateList.length !== 0)? "" : "There is no generated project in the database", 
    }
  }

  handleDeployDialogClose = () => {
    this.setState({
      deployGeneratedProjectDialog: false,
    }, () => {
      this.setState({
        deployGeneratedProjectContent: "",
        deployGeneratedProjectWebhook: "",
        deployDockerImageStage: "",
      })
    })
  }

  render() {
    const { classes } = this.props;
    let { generatedProjectTemplateList, listInfoText } = this.getGeneratedProjectTemplateList(this.state.generatedProjectList, classes);
    let downloadImageContent;
    if (this.state.downloadDockerImageLoading) {
      downloadImageContent = <CircularProgress className={classes.progress} />
    } else {
      downloadImageContent = <div> The download will start soon.</div>
    }

    let deployChatbotContent;
    if (!this.state.deployGeneratedProjectContent) {
      deployChatbotContent = <div> The Chatbot WebHook is being generated. Please wait ... </div>
    } else {
      deployChatbotContent = <div>
          <div>The webhook of the deployed chatbot is: "{this.state.deployGeneratedProjectWebhook}"</div>
          <br></br>
          <div>
            In order to use it you have to:
            <ol>
              <li>Copy the webhook</li>
              <li>Inserted into the fullfillment of the dialogFlow agent</li>
              <li>Congratulations, Your chatbot is ready to be used</li>
            </ol> 
          </div>
        </div>
    }
    
    return (
      <div className="ChatBotReviewer">
        <div className={classes.demo}>
          <List>
            <div>{listInfoText}</div>
            {
              generatedProjectTemplateList
            }
          </List>

          <Dialog open={this.state.downloadDockerImageDialog} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">This may take a while. Please wait ...</DialogTitle>
            <DialogContent>
              <div> {this.state.downloadDockerImageStage} </div>
              {
                downloadImageContent
              }
            </DialogContent>
          </Dialog>
          
          <Dialog open={this.state.deployGeneratedProjectDialog} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Chatbot Deploy ...</DialogTitle>
            <DialogContent>
            <div> {this.state.deployDockerImageStage} </div>
              {
                deployChatbotContent
              }
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleDeployDialogClose} color="primary">
                Cancel
              </Button>
            </DialogActions>
          </Dialog>

        </div>
      </div>
    );
  }
}

ChatBotReviewer.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styledComponent = withStyles(styles)(ChatBotReviewer);
export default styledComponent;
