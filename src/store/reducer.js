const defaultTheme = {
	dashboard: {
		backgroundColor: '#FFFFFF',
		
	}
}

const appTheme = {
	"light": {
		dashboard: {
			backgroundColor: '#FFFFFF',
			width: window.innerWidth,
			height: window.innerHeight,
		}
	},
	"dark": {
		dashboard: {
			backgroundColor: '#404040',
			width: window.innerWidth,
			height: window.innerHeight,
		}
	}
}

const initialState = {
	generatedChatBotList: {},
	deployedChatBotList: {},
	
	diagramModelList: {},
	savedDiagramModelList: {},
	
	dialogFlowIntentList: {},
	updatedIntentList: {},

	previewDiagramDialog: false,
	dialogFlowConfigDialog: false,
	selectedDialogFlowIntent: null,
	selectedDiagram: null,
	appComponent: null,
	appTheme: defaultTheme,

	imagesBeingUploaded: 0,

	appSettings: {
		dialogFlowConfig: {
			status: "unknown",
			dialflowAgentToken: "",
		},
		lineChatBotConfig: {
			status: "unknown",
			lineChannelId: "",
			lineChannelSecret: "",
			lineChannelAccessToken: "",
		}
	}
}

const addToDiagramModelList = (state, diagramName, diagramModelJson) => {
	// let newState = {...state};
	let newDiagramModelList = { ...state.diagramModelList }
	newDiagramModelList[diagramName] = diagramModelJson;
	return newDiagramModelList;
}

const deleteFromDiagramModelList = (state, diagramName) => {
	let newDiagramModelList = { ...state.diagramModelList }
	delete newDiagramModelList[diagramName];
	return newDiagramModelList;
}

const addToSavedDiagramModelList = (state, diagramName, diagramModelJson) => {
	let newSavedDiagramModelList = { ...state.savedDiagramModelList };
	newSavedDiagramModelList[diagramName] = diagramModelJson;
	return newSavedDiagramModelList;
}

const addToDialogFlowIntentList = (state, intentID, intentName, trainingPhrasesList) => {
	let newDialogFlowIntentList = { ...state.dialogFlowIntentList };
	let intent = {}
	intent["trainingPhrases"] = trainingPhrasesList;
	intent["id"] = intentID;
	newDialogFlowIntentList[intentName] = intent;
	return newDialogFlowIntentList;
}

const addToUpdatedIntentList = (state, intentName, trainingPhrasesList) => {
	let newUpdatedIntentList = { ...state.updatedIntentList };
	let intent = {}
	let intentID;
	let oldIntent = state.dialogFlowIntentList[intentName];
	if (oldIntent == null || oldIntent["id"] == null) {
		intentID = null;
	} else {
		intentID = oldIntent["id"];
	}
	intent["trainingPhrases"] = trainingPhrasesList;
	intent["id"] = intentID;
	newUpdatedIntentList[intentName] = intent;
	return {
		updatedIntentList: newUpdatedIntentList,
		dialogFlowIntentList: addToDialogFlowIntentList(
			state,
			intentID,
			intentName,
			trainingPhrasesList
		)
	};
}

const setAppTheme = (currentAppTheme) => {
	if (currentAppTheme === "dark") {
		return appTheme.light;
	} else {
		return appTheme.dark;
	}
}

const reducer = (state = initialState, action) => {
	switch (action.type) {

		case 'setAppComponent':
			return {
				...state,
				appComponent: action.appComponent
			}

		case 'setDiagramModelList':
			return {
				...state,
				diagramModelList: action.diagramModelList
			}

		case 'addToDiagramModelList':
			return {
				...state,
				diagramModelList: addToDiagramModelList(state, action.diagramName, action.diagramModelJson)
			}

		case 'removeFromDiagramModelList':
			return {
				...state,
				diagramModelList: deleteFromDiagramModelList(state, action.diagramName)
			}

		case 'setSavedDiagramModelList':
			return {
				...state,
				savedDiagramModelList: action.savedDiagramModelList
			}

		case 'addToSavedDiagramModelList':
			return {
				...state,
				savedDiagramModelList: addToSavedDiagramModelList(state, action.diagramName, action.diagramModelJson)
			}

		case 'setPreviewDiagramDialog':
			return {
				...state,
				previewDiagramDialog: action.previewDiagramDialog
			}

		case 'setSelectedDiagram':
			return {
				...state,
				selectedDiagram: action.selectedDiagram
			}
		
		case 'addToDialogFlowIntentList':
			return {
				...state,
				dialogFlowIntentList: addToDialogFlowIntentList(
					state,
					action.intentID,
					action.intentName,
					action.trainingPhrasesList
				)
			}
		
		case 'addToUpdatedIntentList':
			let { updatedIntentList, dialogFlowIntentList } = addToUpdatedIntentList(
				state,
				action.intentName,
				action.trainingPhrasesList
			);
			return {
				...state,
				updatedIntentList,
				dialogFlowIntentList
			}

		case 'setDialogFlowConfigDialog':
			return {
				...state,
				dialogFlowConfigDialog: action.dialogFlowConfigDialog
			}

		case 'setSelectedDialogFlowIntent':
			return {
				...state,
				selectedDialogFlowIntent: action.selectedDialogFlowIntent
			}
		
		case 'resetUpdatedIntentList':
			return {
				...state,
				updatedIntentList: {}
			}

		case 'plusImagesBeingUploaded':
			return {
				...state,
				imagesBeingUploaded: state.imagesBeingUploaded + 1
			}
		
		case 'minusImagesBeingUploaded':
			return {
				...state,
				imagesBeingUploaded: state.imagesBeingUploaded - 1
			}
		
		case 'setAppTheme':
			return {
				...state,
				appTheme: setAppTheme(action.appTheme)
			}
		
		case 'updateDialogFlowConfig':
			return {
				...state,
				appSettings: {
					...state.appSettings,
					dialogFlowConfig: action.dialogFlowConfig
				}
			}
		
		case 'updateLineChatBotConfig':
			return {
				...state,
				appSettings: {
					...state.appSettings,
					lineChatBotConfig: action.lineChatBotConfig
				}
			}

		default:
			return state;
	}
}

export default reducer;