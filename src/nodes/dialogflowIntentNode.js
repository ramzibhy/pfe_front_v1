import { makePort } from './nodeFunctionalities';
import store from "../store/store";
import * as go from "gojs";

const dialogflowintentNode = () => {
    const $ = go.GraphObject.make;
    return (
        $(go.Node, "Auto",
            { locationSpot: go.Spot.Center,
            contextMenu: $(go.Adornment, "Vertical",
                new go.Binding("itemArray", "commands"),
                {
                    itemTemplate:
                    $("ContextMenuButton",
                        $(go.TextBlock, new go.Binding("text")),
                        {
                            click: function (e, button) {
                                let intentTextBlock = button.part.adornedPart.findObject("intentTextBlock");
                                // 
                                store.dispatch({type: "setSelectedDialogFlowIntent", selectedDialogFlowIntent: intentTextBlock.text});
                                store.dispatch({type: "setDialogFlowConfigDialog", dialogFlowConfigDialog: true});
                            }
                        }
                    )
                }
            )
            },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Shape, "RoundedRectangle", {
                fill: "#F85B4F",
                width: 130,
            }),
            $(go.Panel, "Vertical", {
                margin: 5,
            },
                $(go.TextBlock, {
                    margin: new go.Margin(3, 0, 10, 0),
                    maxSize: new go.Size(100, 30),
                    isMultiline: false,
                    font: "bold 10pt sans-serif",
                    text: "DialogFlow Webhook",
                }),
                $(go.TextBlock, {
                    text: "intent:",
                    font: "bold 10pt sans-serif",
                }),
                $(go.TextBlock, {
                    text: "Edit This",
                    editable: true, isMultiline: true,
                    width: 150, wrap: go.TextBlock.WrapFit,
                    margin: new go.Margin(0, 0, 10, 0),
                    name: "intentTextBlock"
                    },
                    new go.Binding("text", "dialogflowIntent").makeTwoWay(),
                ),
            ),
            makePort($, "T", go.Spot.Top, go.Spot.Top, true, false),
            makePort($, "B", go.Spot.Bottom, go.Spot.Bottom, false, true),
        )
    );
}

export default dialogflowintentNode;