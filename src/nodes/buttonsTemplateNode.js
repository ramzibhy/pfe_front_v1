import { nodeStyle, makePort } from './nodeFunctionalities';
import * as go from "gojs";

import { uploadImageContextMenuFunction } from './nodeCommonFunctions';

const buttonsTemplateNode = (_this) => {
    const $ = go.GraphObject.make;
    return (
        $(go.Node, "Auto", nodeStyle(),
            { locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Shape, "RoundedRectangle", {
                fill: "#E774C8",
                width: 130,
            }),
            $(go.Panel, "Vertical",{
                margin: 5
            },
                $(go.TextBlock, {
                    margin: new go.Margin(3, 0, 10, 0),
                    width: 100,
                    isMultiline: false,
                    font: "bold 10pt sans-serif",
                    text: "Buttons Template"
                }),
                $(go.Picture, 
                    new go.Binding("source", "thumbnailImageUrl").makeTwoWay(),
                    {
                    maxSize: new go.Size(110, 70),
                    source: "images/default2.png",
                    margin: new go.Margin(0, 0, 2, 0),
                    name: "thumbnailImageUrl",
                    contextMenu: $(go.Adornment, "Vertical",
                        new go.Binding("itemArray", "commands"),
                        {
                            itemTemplate:
                            $("ContextMenuButton",
                                $(go.TextBlock, new go.Binding("text")),
                                {
                                    click: function (e, button) {
                                        uploadImageContextMenuFunction(button, 'fileUpload2', _this);
                                    }
                                }
                            )
                        }
                    )
                }),
                $(go.TextBlock, {
                    text: "Title:",
                    font: "bold 10pt sans-serif",
                }),
                $(go.TextBlock, {
                    text: "Edit This (Null)",
                    editable: true, isMultiline: true,
                    width: 150, wrap: go.TextBlock.WrapFit,
                    margin: new go.Margin(0, 0, 10, 0),
                    },
                    new go.Binding("text", "title").makeTwoWay(),
                ),
                $(go.TextBlock, {
                    text: "Text:",
                    font: "bold 10pt sans-serif",
                }),
                $(go.TextBlock, {
                    text: "Edit This",
                    editable: true, isMultiline: true,
                    width: 100, wrap: go.TextBlock.WrapFit,
                    margin: new go.Margin(0, 0, 10, 0),
                    },
                    new go.Binding("text", "text").makeTwoWay(),
                ),
                /*
                $(go.TextBlock, {
                    text: "Image Aspect Ratio:",
                    font: "bold 10pt sans-serif",
                    maxSize: new go.Size(100, 40),
                }),
                $(go.TextBlock, {
                    text: "Edit This (Null)",
                    editable: true, isMultiline: true,
                    width: 150, wrap: go.TextBlock.WrapFit,
                    margin: new go.Margin(0, 0, 10, 0),
                    },
                    new go.Binding("text", "imageAspectRatio").makeTwoWay(),
                ),
                $(go.TextBlock, {
                    text: "Image Size:",
                    font: "bold 10pt sans-serif",
                }),
                $(go.TextBlock, {
                    text: "Edit This (Null)",
                    editable: true, isMultiline: true,
                    width: 150, wrap: go.TextBlock.WrapFit,
                    margin: new go.Margin(0, 0, 10, 0),
                    },
                    new go.Binding("text", "imageSize").makeTwoWay(),
                ),
                $(go.TextBlock, {
                    text: "ImageBackgroundColor:",
                    font: "bold 10pt sans-serif",
                    maxSize: new go.Size(100, 40),
                }),
                $(go.TextBlock, {
                    text: "Edit This (Null)",
                    editable: true, isMultiline: true,
                    width: 100, wrap: go.TextBlock.WrapFit,
                    },
                    new go.Binding("text", "imageBackgroundColor").makeTwoWay(),
                ),
                */
            ),
            makePort($, "T", go.Spot.Top, go.Spot.Top, true, false),
            makePort($, "B", go.Spot.Bottom, go.Spot.Bottom, false, true),
        )
    );
}

export default buttonsTemplateNode;