import { makePort } from './nodeFunctionalities';
import * as go from "gojs";

const botControllerClassNode = () => {
    const $ = go.GraphObject.make;
    return (
        $(go.Node, "Auto",
            { locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Shape, "RoundedRectangle", {
                fill: "#9CCCFC",
                width: 130,
            }),
            $(go.Panel, "Vertical", {
                margin: 5,
            },
                $(go.TextBlock, {
                    margin: new go.Margin(3, 0, 10, 0),
                    maxSize: new go.Size(100, 30),
                    isMultiline: false,
                    font: "bold 10pt sans-serif",
                    text: "ChatBot Controller Class",
                }),
                $(go.TextBlock, {
                    text: "Class name:",
                    font: "bold 10pt sans-serif",
                }),
                $(go.TextBlock, {
                    text: "Edit This",
                    editable: true, isMultiline: true,
                    width: 150, wrap: go.TextBlock.WrapFit,
                    margin: new go.Margin(0, 0, 10, 0),
                    },
                    new go.Binding("text", "className").makeTwoWay(),
                ),
            ),
            makePort($, "B", go.Spot.Bottom, go.Spot.Bottom, false, true),
        )
    );
}

export default botControllerClassNode;