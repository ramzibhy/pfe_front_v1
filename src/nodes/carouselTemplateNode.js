import { makePort } from './nodeFunctionalities';
import * as go from "gojs";

const carouselTemplateNode = () => {
    const $ = go.GraphObject.make;
    return (
        $(go.Node, "Auto",
            { locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Shape, "RoundedRectangle", {
                fill: "#E774C8",
                width: 130,
            }),
            $(go.Panel, "Vertical", {
                margin: 5,
            },
                $(go.TextBlock, {
                    margin: new go.Margin(3, 0, 10, 0),
                    maxSize: new go.Size(100, 40),
                    font: "bold 10pt sans-serif",
                    text: "Carousel Template",
                }),
                /*
                $(go.TextBlock, {
                    text: "Image Aspect Ratio:",
                    font: "bold 10pt sans-serif",
                    maxSize: new go.Size(100, 40),
                }),
                $(go.TextBlock, {
                    text: "Edit This (Null)",
                    editable: true, isMultiline: true,
                    width: 150, wrap: go.TextBlock.WrapFit,
                    margin: new go.Margin(0, 0, 10, 0),
                    },
                    new go.Binding("text", "imageAspectRatio").makeTwoWay(),
                ),
                $(go.TextBlock, {
                    text: "Image Size:",
                    font: "bold 10pt sans-serif",
                }),
                $(go.TextBlock, {
                    text: "Edit This (Null)",
                    editable: true, isMultiline: true,
                    width: 150, wrap: go.TextBlock.WrapFit,
                    margin: new go.Margin(0, 0, 10, 0),
                    },
                    new go.Binding("text", "imageSize").makeTwoWay(),
                ),
                */
            ),
            makePort($, "T", go.Spot.Top, go.Spot.Top, true, false),
            makePort($, "B", go.Spot.Bottom, go.Spot.Bottom, false, true),
        )
    );
}

export default carouselTemplateNode;