import axios from 'axios';
import Store from '../store/store';


let uploadImageContextMenuFunction = (button, inputField, diagramObject) => {
    let appStore = Store.getState();
    let appComponent = appStore.appComponent;

    let cmd = button.data;
    let nodedata = button.part.adornedPart.data;
    let pictureObject = button.part.adornedPart.findObject("thumbnailImageUrl");
    const getImage = document.getElementById(inputField);
    //  
    // ----------- 
    if(cmd.action == "change") {
        getImage.onchange = (e) => {
            let file = e.target.files[0];
            let reader = new FileReader();
            reader.onloadend = () => {
                pictureObject.source = reader.result;

                //----- Note of image start Uploading---------------------
                // diagramObject.setState({imagesBeingUploaded: diagramObject.state.imagesBeingUploaded + 1});
                //  
                Store.dispatch({
                    type: 'plusImagesBeingUploaded'
                });
                appComponent.props.enqueueSnackbar("An image is being uploaded.", {variant: 'warning'});
                //--------------------------------------------------

                axios({
                    url: `${process.env.REACT_APP_BACKEND_IMAGE_UPLOAD_SERVICE}/upload`,
                    data: {picture: reader.result},
                    method: 'post',
                    config: {headers: {'Content-Type': 'application/json'}},
                })
                .then((response) => {
                    // TODO: maybe I should some treatement outside of the callback, i will test it later on
                     
                    var img = new Image();
                    img.onload = function() { 
                        pictureObject.source = response.data.filepath;
                         

                        //----- Note of image done Uploading---------------------
                        // diagramObject.setState({imagesBeingUploaded: diagramObject.state.imagesBeingUploaded - 1});
                        //  
                        Store.dispatch({
                            type: 'minusImagesBeingUploaded'
                        });
                        appComponent.props.enqueueSnackbar("Image done uploading.", {variant: 'success'});
                        //--------------------------------------------------
                     }
                    img.src = response.data.filepath;
                })
                .catch((error) => {
                    Store.dispatch({
                        type: 'minusImagesBeingUploaded'
                    });
                    appComponent.props.enqueueSnackbar("There is an error while uploading the image the image");
                    console.log(error)
                });
                /*
               bodyData.set('picture', reader.result);
                
               axios({
                   url: "http://127.0.0.1:5000/post",
                   data: {picture: "hello"},
                   method: 'post',
                   config: {headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
               })
               .then((res)=>{console.log(res)})
               .catch((err)=>{console.log(err)})
                //pictureObject.source = reader.result;*/
            }
            reader.readAsDataURL(file);
        }
        getImage.click();
    } else if (cmd.action == "reset") {
        getImage.value = '';
        pictureObject.source = "images/default2.png"
    }
}

export {
    uploadImageContextMenuFunction
}
