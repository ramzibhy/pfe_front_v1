// ----------------- Returned Data Structures ------------------

// in case JSONObject doesn't mapping automaticlly and guess what it does not xD
let stringOrNull = (value) => {
    return (value != null && value != "")? "\""+value+"\"":null;
}

let stringOrError = (info, value) => {
    if (value != null && value != "") {
        return "\""+value+"\"";
    }
    throw `"${info}" should not be null`;
}

//TODO : maybe change stringOrNull to stringOrNull
let notStringOrNull = (value) => {
    return (value != null)? value:null;
}

let notStringOrError = (info, value) => {
    if (value != null) {
        return value;
    }
    throw `"${info}" should not be null`;
}

let replaceSpecialCharacters = (data) => {
    /*
    const reg = /[^\w\s]/g;
    let specialCharactersList = data.match(reg);
    specialCharactersList = Array.from(new Set(specialCharactersList));
    specialCharactersList.forEach((specialCharacter) => {
        data = data.replace(new RegExp(specialCharacter,"g"), `\\${specialCharacter}`);
         
    })
    */
    return data.replace(/["]/g, '\\"');
}

// ----------------- Verify Limitations ------------------
let verifyActionsLimit = (info, actionsList, limit) => {
    if (actionsList.length > limit) {
        throw `"${info}" can't have more than ${limit} actions`;
    }
}

let verifyColumnsLimit = (info, columnsList, limit) => {
    if (columnsList.length > limit) {
        throw `"${info}" can't have more than ${limit} columns`;
    }
}

let verifyConfirmTemplateActionsNumber = (confirmTemplateData) => {
    if (confirmTemplateData.actions.length != process.env.REACT_APP_CONFIRM_TEMPLATE_ACTIONS_NUMBER) {
        throw `"Confirm Template" can only have exactly ${process.env.REACT_APP_CONFIRM_TEMPLATE_ACTIONS_NUMBER} actions`;
    }
}

let verifyConfirmTemplateList = (confirmTemplateList) => {
    confirmTemplateList.forEach((confirmTemplateData) => {
        verifyConfirmTemplateActionsNumber(confirmTemplateData);
        // other verifications of the confirm template can be implemented here
    });
}

let verifyCarouselTemplateColumnsNumber = (carouselTemplateData) => {
    if(carouselTemplateData.columns.length > process.env.REACT_APP_CAROUSEL_TEMPLATE_COLUMNS_NUMBER) {
        throw `"Carousel Template" can only have at max ${process.env.REACT_APP_CAROUSEL_TEMPLATE_COLUMNS_NUMBER} columns`;
    }
}

let verifyCarouselTemplateList = (carouselTemplateList) => {
    carouselTemplateList.forEach((carouselTemplateData) => {
        verifyCarouselTemplateColumnsNumber(carouselTemplateData);
        // other verifications of the carousel template can be implemented here
    })
}



// ----------------------------------------------------------------------

export {
    stringOrNull,
    stringOrError,
    notStringOrNull,
    notStringOrError,

    verifyActionsLimit,
    verifyColumnsLimit,
    verifyConfirmTemplateActionsNumber,
    verifyConfirmTemplateList,
    verifyCarouselTemplateColumnsNumber,
    verifyCarouselTemplateList
}