import {
    stringOrNull,
    stringOrError,
    notStringOrNull,
    notStringOrError
    } from "./verificationFunctions";

// ------- Verification functions for every attribute in the node -------

// ----------------- Verify Attributes ----------------------
// next is the function that going to return the appropriate data structure
// exemple of next is stringOrNull/stringOrError/notStringOrNull/notStringOrError
// pred can be any custom treatement before testing the conditions of our value
// next was replaced with pred because we need to test if our value is null or not before testing the other conditions
let verifyMaxCharacters = (info ,value, length) => {
    if (value.length > length) {
        throw `"${info}" length should not pass ${length}`;
    }
}

let verifyValidURI = (info, value) => {
    const httpReg = /^https{0,1}:\/\//;
    const telReg = /^\+{0,1}[0-9]+$/;
    if(!(httpReg.test(value) || telReg.test(value))) {
        throw `"${info}" is not a valid URI`;
    }
}

let stringOrError_verifyMaxCharacters = (info, value, max) => {
    let returnedValue = stringOrError(info, value);
    verifyMaxCharacters(info, value, max);
    return returnedValue;
}

let stringOrNull_verifyMaxCharacters = (info, value, max) => {
    let returnedValue = stringOrNull(value);
    if(returnedValue != null) {
        verifyMaxCharacters(info, value, max);
    }
    
    return returnedValue;
}

let stringOrError_verifyValidURI = (info, value) => {
    let returnedValue = stringOrError(info, value);
    verifyValidURI(info, value);
    return returnedValue;
}

let verifyTextMessage_text = (info, value) => {
    return stringOrError_verifyMaxCharacters(info, value, process.env.REACT_APP_MESSAGE_TEXT_MAX_CHARACTERS);
}

let verifyTemplateMessage_altText = (info, value) => {
    return stringOrError_verifyMaxCharacters(info, value, process.env.REACT_APP_TEMPLATE_ALT_TEXT_MAX_CHARACTERS);
}

let verifyTemplate_title = (info, value) => {
    return stringOrNull_verifyMaxCharacters(info, value, process.env.REACT_APP_TEMPLATE_TITLE_MAX_CHARACTERS);
}

let verifyTemplate_text = (info, value) => {
    return stringOrError_verifyMaxCharacters(info, value, process.env.REACT_APP_TEMPLATE_TEXT_MAX_CHARACTERS);
}

let verifyCarouselColumn_title = (info, value) => {
    return stringOrNull_verifyMaxCharacters(info, value, process.env.REACT_APP_TEMPLATE_TITLE_MAX_CHARACTERS);
}

let verifyCarouselColumn_text = (info, value) => {
    return stringOrError_verifyMaxCharacters(info, value, process.env.REACT_APP_TEMPLATE_TEXT_MAX_CHARACTERS);
}

let verifyAction_label = (info, value) => {
    return stringOrError_verifyMaxCharacters(info, value, process.env.REACT_APP_ACTION_LABEL_MAX_CHARACTERS);
}

let verifyPostBackAction_data = (info, value) => {
    return stringOrError_verifyMaxCharacters(info, value, process.env.REACT_APP_POSTBACK_DATA_MAX_CHARACTERS);
}

let verifyPostBackAction_displayText = (info, value) => {
    return stringOrNull_verifyMaxCharacters(info, value, process.env.REACT_APP_POSTBACK_DISPLAY_TEXT_MAX_CHARACTERS);
}

let verifyPostBackAction_text = (info, value) => {
    return stringOrNull_verifyMaxCharacters(info, value, process.env.REACT_APP_ACTION_TEXT_MAX_CHARACTERS);
}

let verifyUriAction_uri = (info, value) => {
    return stringOrError_verifyValidURI(info, value);
}

let verifyMessageAction_text = (info, value) => {
    return stringOrError_verifyMaxCharacters(info, value, process.env.REACT_APP_ACTION_TEXT_MAX_CHARACTERS);
}

export {
    verifyTextMessage_text,
    verifyTemplateMessage_altText,
    verifyTemplate_title,
    verifyTemplate_text,
    verifyCarouselColumn_title,
    verifyCarouselColumn_text,
    verifyAction_label,
    verifyUriAction_uri,
    verifyMessageAction_text,
    verifyPostBackAction_data,
    verifyPostBackAction_displayText,
    verifyPostBackAction_text
}