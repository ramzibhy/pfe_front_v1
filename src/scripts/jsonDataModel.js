import {stringOrNull,
    stringOrError,
    notStringOrNull,
    notStringOrError
} from "./verificationFunctions";

import {
    createNewIntent,
    createUpdateIntent
} from "./dialogflowFunctions";

import {
    addUnconfiguredIntents
} from "./nodeConfigurationFunctions";

import {
    verifyActionsLimit,
    verifyColumnsLimit,
    verifyConfirmTemplateActionsNumber,
    verifyConfirmTemplateList,
    verifyCarouselTemplateColumnsNumber,
    verifyCarouselTemplateList
} from "./verificationFunctions";

import {
    verifyTextMessage_text,
    verifyTemplateMessage_altText,
    verifyTemplate_title,
    verifyTemplate_text,
    verifyCarouselColumn_title,
    verifyCarouselColumn_text,
    verifyAction_label,
    verifyUriAction_uri,
    verifyMessageAction_text,
    verifyPostBackAction_data,
    verifyPostBackAction_displayText,
    verifyPostBackAction_text
} from "./lineAttributesVerification";

let createNodeFromCategory = (data) => {
    switch (data.category) {
        case "BotControllerClassNode":
            return createChatBotController(data);
        case "DialogflowintentNode":
            return createMessage(data);
        case "TemplateMessageNode":
            return createTemplateMessage(data);
        case "TextMessageNode":
            return createTextMessage(data);
        case "CarouselTemplateNode":
            return createCarouselTemplate(data);
        case "CarouselColumnNode":
            return createCarouselColumn(data);
        case "ButtonTemplateNode":
            return createButtonsTemplate(data);
        case "ConfirmTemplateNode":
            return createConfirmTemplate(data);
        case "MessageActionNode":
            return createMessageAction(data);
        case "UriActionNode":
            return createUriAction(data);
        case "PostBackActionNode":
            return createPostBackAction(data);
        default:
            throw "category of the node not found, nani !!";

    }
}

let makeLinksBetweenNodes = (data, links) => {
    let confirmTemplateToVerify = [];
    let carouselTemplateToVerify = [];

    links.forEach((link) => {
        let from = link.from;
        let to = link.to;
        if ( data[to].dataType == "class" && data[from].dataType == "message" ) {
            data[to].responses.push(data[from]);
            //continue;
        }
        else if ( data[to].dataType == "message" && data[from].dataType == "messageType" ) {
            data[to].messageType = data[from].messageType;
            data[to].messageInfo = data[from];
            //continue;
        }
        else if ( data[to].dataType == "messageType" && data[from].dataType == "templateType" ) {
            data[to].templateType = data[from].templateType;
            data[to].templateData = data[from];
            //continue;
        }
        else if ( data[to].dataType == "templateType" && data[from].dataType == "templateColumn" ) {
            data[to].columns.push(data[from]);

            if( data[to].templateType == "carousel" && !carouselTemplateToVerify.includes(data[to])) {
                carouselTemplateToVerify.push(data[to]);
            }
            //treatement of other template types can be added here
            //continue;
        }
        else if ( data[to].dataType == "templateColumn" && data[from].dataType == "action" ) {
            data[to].actions.push(data[from]);
            //continue;
        }
        else if ( data[to].dataType == "templateType" && data[from].dataType == "action" ) {
            data[to].actions.push(data[from]);

            if( data[to].templateType == "confirm" && !confirmTemplateToVerify.includes(data[to])) {
                confirmTemplateToVerify.push(data[to]);
            }
            //treatement of other template types can be added here
            //continue;
        }
        else {
            throw `Relation between "${data[to].dataType}" and "${data[from].dataType}" is invalid`;
        }
    });
    verifyConfirmTemplateList(confirmTemplateToVerify);
     
    verifyCarouselTemplateList(carouselTemplateToVerify);
    return data;
}

let getChatBotClasses = (data) => {
    let classes = [];
    let dataKeys = Object.keys(data);
    dataKeys.forEach((key)=>{
        if(data[key].dataType == "class") {
            classes.push(data[key]);
        }
    });
    return classes;
}

let jsonDataModel = (gojsDataModel) => {
    var gojsDataModel = JSON.parse(gojsDataModel);
    let nodeDataArray = gojsDataModel.nodeDataArray;
    let linkDataArray = gojsDataModel.linkDataArray;

    let nodes = {}
    for (var i = 0; i < nodeDataArray.length; i++) {
        const nodeKey = nodeDataArray[i].key;
        nodes[nodeKey] = createNodeFromCategory(nodeDataArray[i]);
    }
    let linkedNodes = makeLinksBetweenNodes(nodes, linkDataArray);
    return getChatBotClasses(linkedNodes);
}

let createChatBotController = (data) => {
    return {
        dataType: "class",
        className: notStringOrError("Class name",data.className),
        responses: []
    }
}

// ----------------- Creating message ------------------
let createMessage = (data) => {
    
    let intentName = notStringOrError("Dialogflow Intent",data.dialogflowIntent);
    addUnconfiguredIntents(intentName).then(() => {
        createUpdateIntent(intentName);
    })
    
    return {
        dataType: "message",
        messageType: null,
        dialogflowintent: stringOrError("Dialogflow Intent",data.dialogflowIntent),
        messageInfo: {},
    }
}

// ------------------ Creating Text Messages -------------
let createTextMessage = (data) => {
    return {
        dataType: "messageType",
        messageType: "text",
       text: verifyTextMessage_text("Text of a Text message", data.text)
    }
}

// ---------------- Creating Template Message ----------

let createTemplateMessage = (data) => {
    return {
        dataType: "messageType",
        messageType: "template",
        altText: verifyTemplateMessage_altText("AltText of a Template message", data.altText),
        templateType: null,
        templateData: {},
    }
}

// -------------------- Creating Templates ----------------

let createConfirmTemplate = (data) => {
    return {
        dataType: "templateType",
        templateType: "confirm",
        text: verifyTemplate_text("Text of a Confirm template", data.text),
        actions: [],
    }
}

let createButtonsTemplate = (data) => {
    return {
        dataType: "templateType",
        templateType: "buttons",
        thumbnailImageUrl: stringOrNull(data.thumbnailImageUrl),
        title: verifyTemplate_title("Title of Buttons template", data.title),
        text: verifyTemplate_text("Text of Buttons template", data.text),
        actions: [],
    }
}

let createCarouselTemplate = (data) => {
    return {
        dataType: "templateType",
        templateType: "carousel",
        columns: [],
    }
}

// ----------- Creating Carousel Column -----------------

let createCarouselColumn = (data) => {
    return {
        dataType: "templateColumn",
        thumbnailImageUrl: stringOrNull(data.thumbnailImageUrl),
        title: verifyCarouselColumn_title("Title of Carousel column", data.title),
        text: verifyCarouselColumn_text("Text of Carousel column", data.text),
        actions: [],
    }
}

// ----------------- Creating Actions ------------------
let createPostBackAction = (data) => {
    return {
        dataType: "action",
        actionType: "postBack",
        label: verifyAction_label("Label of a PostBack action", data.label),
        data: verifyPostBackAction_data("Data of a PostBack action", data.data),
        displayText: verifyPostBackAction_displayText("Display Text of a PostBack action", data.displayText),
        text: verifyPostBackAction_text("Text of a PostBack action", data.text),
    }
}

let createUriAction = (data) => {
    return {
        dataType: "action",
        actionType: "uri",
        label: verifyAction_label("Label of a Uri action", data.label),
        uri: verifyUriAction_uri("Uri of a Uri action", data.uri),
    }
}

let createMessageAction = (data) => {
    return {
        dataType: "action",
        actionType: "message",
        label: verifyAction_label("Label of a Message action", data.label),
        text: verifyMessageAction_text("Text of a Message action", data.text),
    }
}

export default jsonDataModel;