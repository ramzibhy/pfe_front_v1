import store from "../store/store";

//Add unconfigured intents to the store intent list
let addUnconfiguredIntents = (intentName) => {
    let appStore = store.getState();
     
    let checkIntentPromise = new Promise((resolve, reject) => {
        Object.keys(appStore.updatedIntentList).forEach((key) => {
            if (key == intentName) {
                resolve(true);    
            }
        })
        resolve(false);
    });
    return checkIntentPromise.then((value) => {
        if (!value) {
            let newTrainingPhrasesList;
            if (Object.keys(appStore.dialogFlowIntentList).includes(intentName)) {
                newTrainingPhrasesList = appStore.dialogFlowIntentList[intentName]["trainingPhrases"];
                
            } else {
                newTrainingPhrasesList = [];
            }
            store.dispatch({
                type: "addToUpdatedIntentList",
                intentName: intentName,
                trainingPhrasesList: newTrainingPhrasesList
            });
        }
    })
}

export {
    addUnconfiguredIntents
}