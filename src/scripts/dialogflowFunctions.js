import store from "../store/store";
var axios = require('axios');

var config = {
    headers: {'Authorization': "Bearer 97434113140c4894a41239ac18257bba"}
};

var bodyParameters = {
    "contexts": [],
    "events": [],
    "fallbackIntent": false,
    "name": "intent5",
    "priority": 500000,
    "responses": [
      {
        "action": "",
        "affectedContexts": [],
        "defaultResponsePlatforms": {
          "google": true
        },
        "messages": [
          {
            "speech": "",
            "type": 0
          }
        ],
        "parameters": [],
        "resetContexts": false
      }
    ],
    "templates": [],
    "userSays": [
      {
        "count": 0,
        "data": [
          {
            "text": "five"
          }
        ]
      },
      {
        "count": 0,
        "data": [
          {
            "text": "cinq"
          }
        ]
      }
    ],
    "webhookForSlotFilling": false,
    "webhookUsed": true
}

let createUserSays = (speechList) => {
    let userSays = [];
    speechList.forEach((speech) => {
        let userSay = {
            "count": 0,
            "data": [
              {
                "text": `${speech}`
              }
            ]
          }
        userSays.push(userSay);
    });
    return userSays;
}

let createIntentPostBody = (intentName, userSpeechList) => {
    return {
        "contexts": [],
        "events": [],
        "fallbackIntent": false,
        "name": `${intentName}`,
        "priority": 500000,
        "responses": [
          {
            "action": "",
            "affectedContexts": [],
            "defaultResponsePlatforms": {
              "google": true
            },
            "messages": [
              {
                "speech": "",
                "type": 0
              }
            ],
            "parameters": [],
            "resetContexts": false
          }
        ],
        "templates": [],
        "userSays": createUserSays(userSpeechList),
        "webhookForSlotFilling": false,
        "webhookUsed": true
    }
}

let existingIntentsRequest = () => {
  return axios.get(
      process.env.REACT_APP_DIALOGFLOW_INTENTS_WEBHOOK,
      config
  );
}

let createIntentRequest = (intentName, userSpeechList) => {
  return axios.post(
      process.env.REACT_APP_DIALOGFLOW_INTENTS_WEBHOOK,
      createIntentPostBody(intentName, userSpeechList),
      config
  );
}

let updateIntentRequest = (intentID ,intentName, userSpeechList) => {
  return axios.put(
    `${process.env.REACT_APP_DIALOGFLOW_INTENTS_WEBHOOK}/${intentID}`,
    createIntentPostBody(intentName,  userSpeechList),
    config
  );
}

let checkIntentExistence = (intentName) => {
  return existingIntentsRequest().then((response)=>{
      let existe = false;
      for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].name == intentName) {
              existe = true;
              break;
          }
      }
      return existe;
  });
}

let createNewIntent = (intentName, userSpeechList) => {
    let appStore = store.getState();
    console.log()
    checkIntentExistence(intentName).then((exist) => {
        if(!exist) {
            createIntentRequest(intentName, userSpeechList)
            .then((response) => {
              if (response.status === 200) {
                appStore.appComponent.props.enqueueSnackbar(`The intent ${intentName} was successfully created.`, {variant: 'success'});
              }
            })
            .catch((err) => { 
              appStore.appComponent.props.enqueueSnackbar(`There was an error in the creation of the intent "${intentName}".`, {variant: 'error'});
              throw err;
            })
        }
        else {
            appStore.appComponent.props.enqueueSnackbar(`The intent "${intentName}" already exist !!`, {variant: 'warning'});
        }
    }).catch((err) => {
      appStore.appComponent.props.enqueueSnackbar(`There was an error in checking the existing intents, please check your connection wih the dialog flow api.`, {variant: 'error'});
      throw err;
    })
}

let createUpdateIntent = (intentName) => {
  let appStore = store.getState();
   
  let updatedIntentList = appStore.updatedIntentList;
  let createUpdateIntentPromise = new Promise((resolve, reject) => {
    /*
    Object.keys(updatedIntentList).forEach((key) => {
      if (key === intentName) {
        if (updatedIntentList[key].id == null) {
           
          createIntentRequest(intentName, updatedIntentList[key].trainingPhrases)
          .then((response) => {
            if (response.status === 200) {
              appStore.appComponent.props.enqueueSnackbar(`The intent ${intentName} was successfully created.`, {variant: 'success'});
            }
          })
          .catch((err) => {
            appStore.appComponent.props.enqueueSnackbar(`There was an error in the creation of the intent "${intentName}".`, {variant: 'error'});
            throw err;
          })
        }
        else {
           
          updateIntentRequest(updatedIntentList[key].id, intentName, updatedIntentList[key].trainingPhrases)
          .then((response) => {
            if (response.status === 200) {
              appStore.appComponent.props.enqueueSnackbar(`The intent ${intentName} was successfully updated.`, {variant: 'success'});
            }
          })
          .catch((err) => {
            appStore.appComponent.props.enqueueSnackbar(`There was an error in the update of the intent "${intentName}".`, {variant: 'error'});
            throw err;
          })
        }
      }
    });*/
    if (updatedIntentList[intentName].id == null) {
       
      createIntentRequest(intentName, updatedIntentList[intentName].trainingPhrases)
      .then((response) => {
        if (response.status === 200) {
          appStore.appComponent.props.enqueueSnackbar(`The intent ${intentName} was successfully created.`, {variant: 'success'});
          store.dispatch({
            type: "addToDialogFlowIntentList",
            intentID: response.data["id"],
            intentName: intentName,
            trainingPhrasesList: updatedIntentList[intentName].trainingPhrases
          })
        }
      })
      .catch((err) => {
        appStore.appComponent.props.enqueueSnackbar(`There was an error in the creation of the intent "${intentName}".`, {variant: 'error'});
        throw err;
      })
    }
    else {
       
      updateIntentRequest(updatedIntentList[intentName].id, intentName, updatedIntentList[intentName].trainingPhrases)
      .then((response) => {
        if (response.status === 200) {
          appStore.appComponent.props.enqueueSnackbar(`The intent ${intentName} was successfully updated.`, {variant: 'success'});
        }
      })
      .catch((err) => {
        appStore.appComponent.props.enqueueSnackbar(`There was an error in the update of the intent "${intentName}".`, {variant: 'error'});
        throw err;
      })
    }
    resolve("done");
  });
  createUpdateIntentPromise.then((value) => {
    store.dispatch({ type: "resetUpdatedIntentList"});
  });
}

export {
    createNewIntent,
    createUpdateIntent
}